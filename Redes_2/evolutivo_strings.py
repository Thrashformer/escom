

import random, pygame, sys , numpy
from pygame.locals import *
#import funciones_ev

#           VARIABLES       
FPS = 3
ROJO = (255,0,0)
BLANCO = (255, 255, 255)
NEGRO = (0,0,0)
AZUL =(0,0,220)
TRANSPARENT = (255,0,255)
FONDO = [random.randint(0,255) for i in range(3)]
pygame.init()


#nombre_img = "t"
#nombre_img ="ffdp"
#nombre_img ="flores"
#nombre_img ="trebol"
#nombre_img ="cara"
#nombre_img ="vaca"
#nombre_img = "nubes"
#nombre_img = "tipo"
nombre_img = "wings"
#nombre_img = "zai"

#imagen original es de tipo SURFACE
imagen_original = pygame.image.load(nombre_img+".png")

#es el arreglo con el que trabajaremos
''' variables globales de la imagen,. tam_imagen , imagen_original , imagen_original_string '''
imagen_original_string = pygame.image.tostring(imagen_original,"RGBA")
imagen_original_array = pygame.surfarray.array2d(imagen_original)
tam_imagen = imagen_original.get_size()
print "imagen original.get_size() : ", tam_imagen
#print "type (imagen_original)  ",type (imagen_original)

#buffer_surface = pygame.image.tostring(pantalla , "RGB" )
#imagen_from_arr = pygame.image.frombuffer(buffer_surface, tam_imagen , "RGB")

numero_generaciones = 100000000
FILAS    = tam_imagen[0] #alto
COLUMNAS = tam_imagen[1] #ancho
RGB      = 3   #Canales de la imagen

#Ancho de la vetana
WINDOWWIDTH = tam_imagen[0]
#Alto de la Ventana
WINDOWHEIGHT = tam_imagen[1]
#le mandamos la interfaz donde dibujaremos
TAMANIO_POBLACION = 10
CIRCULOS = 15



#SE QUEDA PARA STRASSEN
def inicializar(tam_poblacion, tam_alelo):
    # el cromosoma tendra tantas posiciones como mida la imagen
    # y seran tantos individuos como quiera el usuario
    poblacion = []

    individuo = ""
    #poblacion = [[chr(random.randint(0,255)) for i in range(tam_alelo)] for j in range(n)]
    for i in range(tam_poblacion):
        for j in range(tam_alelo):
            #individuo = individuo + chr(random.randint(0,255))
            individuo = individuo + chr(255)
        poblacion.append(individuo)
        individuo = ""
    return poblacion

#recibe una lista, y devuelve un string de todos sus elementos
def concatena_individuo (individuo):
    tam_individuo = len(individuo)
    resultado = ""
    for i in range (tam_individuo):
        resultado = resultado + chr(individuo[i])
    return resultado

#SE QUEDA PARA STRASSEN
def imprimirIndividuo(individuo, aptitud):
    print individuo, " --> ", aptitud #, len(individuo)

#SE QUEDA PARA STRASSEN
def imprimirPoblacion(poblacion, aptitudes):
    i = 0
    for individuo in poblacion:
        aptitud = aptitudes[i]
        imprimirIndividuo(individuo, aptitud)
        i = i+1

def seleccionaIndividuo(aptitudes):
    for aptitud in aptitudes: 
        sumaAptitudes = aptitud
        mediaAptitud = sumaAptitudes / TAMANIO_POBLACION
    if mediaAptitud is 0:
        mediaAptitud =1
    valoresEsperados = []
    for aptitud in aptitudes:
        valorEsperado = aptitud / mediaAptitud
        valoresEsperados.append(valorEsperado)
    T = sum(valoresEsperados)
    #print "Suma Ve's= ", T
    #print valoresEsperados
    r = random.random()*T
    suma = 0.0
    i=0
    for ve in valoresEsperados:
        suma = suma + ve
        if suma >= r:
            return i
        i=i+1

def cruza1Punto(padre, madre):
    tam_padre = len(padre)
    #escoge el punto de donde se hara la cruza, va desde la posicion 1 hasta n-1 , osea 83
    puntoCruza = random.randint(1, tam_padre-1)
    pi = padre[0:puntoCruza]
    pd = padre[puntoCruza:tam_padre]

    mi = madre[0:puntoCruza]
    md = madre[puntoCruza:tam_padre]
    hijo1 = pi + md
    hijo2 = mi + pd
    return hijo1, hijo2

''' recibe un string '''
''' mutamos dos bits del individuo '''
def mutacion(individuo):
    tam_individuo = len(individuo)
    for i in range (2):
        puntoMutacion = random.randint( 1 , tam_individuo-1)
        #puntoMutacion = random.randint(0, 55)
        #print "Punto Mutacion: ", puntoMutacion
        individuo = individuo[:puntoMutacion-1] + chr(random.randint(0, 255)) + individuo[puntoMutacion+1:]

#       imagen_array es de tipo STRING
def calcular_fitness(imagen_string):
    tam_imagen = len(imagen_string)
    #print "calcular_fitness --- tam_imagen : ", tam_imagen
    resultado = 0
    for i in range(tam_imagen):
        if (type(imagen_string[i]) is int) and (type(imagen_original_string[i]) is int) :
            resultado = resultado + abs( imagen_string[i] - imagen_original_string[i] )
        if type(imagen_string[i]) is int :
            #print "se encontro un entero en imagen_String"
            resultado = resultado + abs( imagen_string[i] - ord(imagen_original_string[i]) )
        elif type(imagen_original_string[i]) is int :
            #print "se encontro un entero en imagen_original"
            resultado = resultado + abs( ord(imagen_string[i]) - imagen_original_string[i] )            
        else:
            resultado = resultado + abs( ord(imagen_string[i]) - ord(imagen_original_string[i]) )
    return resultado

#   devuelve una lista de numeros enteros
#   con el desempeno de los individuos
#   el parametro poblacion es de tipo ARRAY
def calcular_fitness_poblacion(poblacion):
    tam_poblacion = len(poblacion)
    fitness_poblacion = []
    for i in range(tam_poblacion):
        fitness_poblacion.append(calcular_fitness(poblacion[i]))
    return fitness_poblacion

#                   ordenar Poblacion
#       poblacion es una lista de ARRAYS
#   la ordenamos segun la funcion de aptitud, y la ordenamos de modo decendente
def ordenarPoblacion(poblacion):
    return sorted(poblacion, key=calcular_fitness, reverse=False)

def evolucionar(poblacion, aptitudes):
    nuevaPoblacion = []
    for i in range(TAMANIO_POBLACION/2):
        posicion1 = seleccionaIndividuo(aptitudes)
        posicion2 = seleccionaIndividuo(aptitudes)
        padre = poblacion[posicion1]        
        madre = poblacion[posicion2]
        (hijo1, hijo2) = cruza1Punto(padre, madre)
        mutacion(hijo1)
        mutacion(hijo2)
        nuevaPoblacion.append(hijo1 )
        nuevaPoblacion.append(hijo2 )
    return nuevaPoblacion


def evolucionar2 (poblacion , aptitudes):
    nuevaPoblacion = []
    for i in range(TAMANIO_POBLACION/2):
        posicion1 = seleccionaIndividuo(aptitudes)
        posicion2 = seleccionaIndividuo(aptitudes)
        padre = poblacion[posicion1]        
        madre = poblacion[posicion2]
        (hijo1, hijo2) = cruza1Punto(padre, madre)

        '''implementamos la mutacion de hacer un circulo en la imagen '''
        aux_pantalla = pygame.image.frombuffer(hijo1, tam_imagen ,"RGBA")
        pintar_circulo_img(aux_pantalla)
        pintar_circulo_img(aux_pantalla)
        hijo1 = pygame.image.tostring(aux_pantalla,"RGBA")

        aux_pantalla = pygame.image.frombuffer(hijo2, tam_imagen ,"RGBA")
        pintar_circulo_img(aux_pantalla)
        pintar_circulo_img(aux_pantalla)
        hijo2 = pygame.image.tostring(aux_pantalla,"RGBA")

        mutacion(hijo1)
        mutacion(hijo2)
        nuevaPoblacion.append(hijo1 )
        nuevaPoblacion.append(hijo2 )
    return nuevaPoblacion    


def mutar_imagen(imagen):
    pintar_circulo_img(imagen)

#   poblacion es de tipo SURFACE
#   le dibujaremos 20 circulos de tamano random
def mutar_poblacion(poblacion):
    tam_poblacion = len(poblacion)
    for i in range(tam_poblacion):
        mutar_imagen(poblacion[i])
    return

#   mundo es de tipo SURFACE
def pintar_circulo_img(mundo):
    radio = random.randint(0,COLUMNAS/2)
    c = random.randint(0,254)
    color = (c,c,c,100)
    surf = pygame.Surface((2*radio,2*radio))
    #recibe la interfaz, el color, las coordenadas X,Y del centro del circulo, su radio y su grosor
    surf.fill(TRANSPARENT)    
    surf.set_colorkey(TRANSPARENT)
    pygame.draw.circle(surf ,color , (radio,radio) , radio )
    surf.set_alpha(128)
    #mundo.blit(surf, (random.randint(0, WINDOWWIDTH), random.randint(0, WINDOWHEIGHT), 2*radio, 2*radio))
    mundo.blit(surf, (random.randint(0, WINDOWWIDTH), random.randint(0, WINDOWHEIGHT)) )    
    #pygame.display.flip()
    #circulo = pygame.draw.circle(mundo,color,(random.randint(0,WINDOWWIDTH),random.randint(0,WINDOWHEIGHT)) , radio , 0)
    #print "circulo : ", type(circulo)



'''     #######     INICIO DEL PROGRAMA PRINCIPAL   ######      '''
def main():
    print "Inicio..."
    global FPSCLOCK, pantalla
    FPSCLOCK = pygame.time.Clock()
    '''inicializamos el mundo '''
    pantalla  = pygame.display.set_mode((WINDOWWIDTH, WINDOWHEIGHT))
    '''color de fondo del mundo'''
    #pantalla.fill(FONDO)
    pantalla.fill(BLANCO)    
    pantalla.set_colorkey(TRANSPARENT)
    pantalla.set_alpha(160) 
    pantalla_string  = pygame.image.tostring(pantalla,"RGBA")
    pantalla_fitness = calcular_fitness(pantalla_string)



    tam_alelo = len(pantalla_string)
    #print "len(pantalla_string) : ", len(pantalla_string)
    #print "ord(pantalla_string[0] :", ord(pantalla_string[0])
    #print type (tam_imagen) # tupla


    poblacion = inicializar(TAMANIO_POBLACION , tam_alelo)
    aptitudes = calcular_fitness_poblacion(poblacion)
    #print type(poblacion)
    #print type(poblacion[0])
    #print type(poblacion[0][0]) ,  poblacion[0][0] , ord(poblacion[0][0])
    #print poblacion[0][0:20]
    #print "+++++++++++"
    #print imagen_original_string[0:20]
    pantalla.blit( pygame.image.frombuffer(poblacion[0], tam_imagen ,"RGBA"), (0,0))
    pantalla_fitness = aptitudes[0]
    #print type(pygame.image.frombuffer(poblacion[0], tam_imagen ,"RGBA")) 

    print "poblacion inicializada...."
    print "fitness de poblacion inicial : ", aptitudes[0]
    juego = True
    iteracion = 0
    while juego:
        for event in pygame.event.get(): # event handling loop
            if event.type == QUIT or (event.type == KEYUP and event.key == K_ESCAPE):
                pygame.quit()
                sys.exit()
            if (event.type == KEYUP and event.key == K_f):
                ''' guardar el surface a imagen PNG'''
                pygame.image.save(pantalla , "capturas/prueba_strings.png")
        '''
        print "Padres:"
        imprimirPoblacion(poblacion, aptitudes)
        '''
        #nuevaPoblacion = evolucionar(poblacion, aptitudes)
        nuevaPoblacion = evolucionar2(poblacion, aptitudes)        
        aptitudesNuevaPoblacion = calcular_fitness_poblacion(nuevaPoblacion)
        '''
        print "Hijos:"
        imprimirPoblacion(nuevaPoblacion, aptitudesNuevaPoblacion)
        '''
        todosIndividuos = poblacion + nuevaPoblacion
        todasAptitudes = aptitudes + aptitudesNuevaPoblacion
        #imprimirPoblacion(todosIndividuos,todasAptitudes)
        '''
        print "Desordenados"
        imprimirPoblacion(todosIndividuos, todasAptitudes)
        '''
        poblacionOrdenada = ordenarPoblacion(todosIndividuos)
        '''
        print "Ordenados"
        aptitudesOrdenadas = calcularAptitudPoblacion(poblacionOrdenada)
        imprimirPoblacion(poblacionOrdenada, aptitudesOrdenadas)
        '''
        poblacion = poblacionOrdenada[0:TAMANIO_POBLACION]

        '''
        print "Ordenados"
        imprimirPoblacion(poblacion, aptitudesOrdenadas[0:TAMANIO_POBLACION])
        '''
        aptitudes = calcular_fitness_poblacion(poblacion)

        #   caso que encuentre un mejor resultado
        if pantalla_fitness > aptitudes[0]:
            #print "anterior best : ", pantalla_fitness , "  nuevo best : ", aptitudes[0] , "  iteracion :", iteracion
            pantalla.fill(BLANCO)
            aux_pantalla = pygame.image.frombuffer(poblacion[0], tam_imagen ,"RGBA")
            pantalla.blit( aux_pantalla, (0,0))
            pantalla_fitness = aptitudes[0]

        if (iteracion % 20) == 0:
            print "Generacion: ", iteracion , "mejor resultado : " , pantalla_fitness
            #imprimirIndividuo(poblacion[0], aptitudes[0])
        if (iteracion % 200) == 0 :
                pygame.image.save(pantalla , "capturas/prueba_"+nombre_img+"_"+str(iteracion)+".png")

        '''AL FINAL ACTUALIZAMOS LA PANTALLA '''
        #Actualizamos la pantalla
        #pygame.display.flip()
        pygame.display.update()
        FPSCLOCK.tick(FPS)
        #juego = False
        #print "termino"
        #input()
        iteracion+=1
    pygame.quit()


main()






