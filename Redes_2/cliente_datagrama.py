# -*- coding: utf-8 -*-
__author__ = 'thrashformer'

# Envio de archivos: cliente
#
import numpy as np
from cv2 import *
import cv2
from tkFileDialog import *
import socket

def elegir_archivo():

    #ruta_del_directorio=askdirectory()
    archivo=askopenfile()
    print (type(archivo),archivo)
    return archivo

#Funcion para enviar del cliente al servidor las capturas
def socket_cliente():
    # Creamos una lista con la dirección de
    # la máquina y el puerto donde
    # estara a la escucha
    CONEXION = (socket.gethostname(), 9001)
    ARCHIVO = elegir_archivo()

    # Instanciamos el socket y nos
    # conectamos
    cliente = socket.socket()
    cliente.connect(CONEXION)

    # Abrimos el archivo en modo lectura binaria
    # y leemos su contenido
    #with open(ARCHIVO, "rb") as archivo:
    with ARCHIVO as archivo:
        buffer = archivo.read()
     
    while True:
        # Enviamos al servidor la cantidad de bytes
        # del archivo que queremos enviar
        print "Enviando buffer"
        cliente.send(str(len(buffer)))

        # Esperamos la respuesta del servidor
        recibido = cliente.recv(10)
        if recibido == "OK":
            # En el caso que la respuesta sea la correcta
            # enviamos el archivo byte por byte
            # y salimos del while
            for byte in buffer:
                cliente.send(byte)
            break

def captura_grises():
    cap = cv2.VideoCapture(0)

    diez=10
    capturas=True
    while(capturas):
        # Grabamos frame por frame
        ret, frame = cap.read()
        #
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        print(type(gray))

        # Se muestra la pantalla en escala de grises
        cv2.imshow('frame',gray)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

        diez-=1
        if diez==0:
            print('false')
            capturas=False

    # Se destruye la pantalla
    cap.release()
    cv2.destroyAllWindows()


def main():
    captura_grises()

main()




