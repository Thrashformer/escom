
package servidor_carrito;

import java.net.*;
import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

class Servidor_ftp
{
       private ServerSocket servidor = null; 
       String mensajeRecibido;
       
       public Servidor_ftp() throws IOException
       {
           int puerto=9090;
          // Creamos socket servidor escuchando en el mismo puerto donde se comunica el cliente
          servidor = new ServerSocket( puerto );
          System.out.println( "Esperando recepcion de archivos..." ); 
       } 
       public void iniciarServidor( ArrayList<Producto> productos )
       {
                          // Creamos el socket que atendera el servidor
              try {
                  
                  
                    System.out.println("Le enviamos al cliente los productos siguientes:");
                    for (int i=0 ; i< productos.size();i++)
                    {
                        System.out.println(productos.get(i));
                    }
                    //Socket cliente = servidor.accept();
                    //un cliente se ha conectado
                    //System.out.println("Cliente conectado desde "+cliente.getInetAddress()+":"+cliente.getPort()); 
                    //BufferedReader entrada= new BufferedReader(new InputStreamReader(cliente.getInputStream()));
                    //DataOutputStream salida= new DataOutputStream(cliente.getOutputStream());

                        //Este objeto hace posible el envio de un objeto
                        //de manera serializada.
			ObjectOutputStream out;
			Socket conexion;                                        
                        while (true)
                        {
				conexion=servidor.accept();
				System.out.println("Un cliente se ha conectado");

				out=new ObjectOutputStream(
                                                 conexion.getOutputStream());
                                //En esta linea se envía el objeto completo
                                for(int i=0;i<productos.size();i++)
                                {
                                    out.writeObject(productos.get(i));
                                    out.flush();
                                System.out.println("Se envio producto!");

                                }

                                conexion.close();
			} 
                                        
              } catch (Exception ex) {
                  System.out.println("Error: "+ex.getMessage());
              }

       }//fin inicializarServidor
       
}// fin de la clase



