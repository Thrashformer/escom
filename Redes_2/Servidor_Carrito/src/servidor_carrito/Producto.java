
package servidor_carrito;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Producto implements java.io.Serializable 
{
    private int id;
    private String nombre;
    private int cantidad;
    private double precio;
    private boolean existencia;
        
    public Producto()
    {   
        this.id=0;
        this.nombre= "Default";
        this.cantidad=1;
        this.precio=120.0;
        this.existencia=true;
    }
    
    public Producto(int id, String nombre, int cantidad, double precio )
    {
        this.id=id;
        this.nombre=nombre;
        this.cantidad=cantidad;
        this.precio=precio;
    }
    
    
    public void validar_existencia(Producto p)
    {
        if (p.getCantidad()==0)
        {
            p.setExistencia(false);
        }
    }
    
    public String toString()
    {
        return this.getNombre()+"  cantidad: "+this.getCantidad()+"  precio: "+this.getPrecio()+"" ;
    }
    
    public static void imprimirProductos(ArrayList<Producto> productos)
    {
        for (Producto p : productos)
        {
            System.out.println(p);
        }
    }
   
    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the cantidad
     */
    public int getCantidad() {
        return cantidad;
    }

    /**
     * @param cantidad the cantidad to set
     */
    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    /**
     * @return the precio
     */
    public double getPrecio() {
        return precio;
    }

    /**
     * @param precio the precio to set
     */
    public void setPrecio(double precio) {
        this.precio = precio;
    }

    /**
     * @return the existencia
     */
    public boolean isExistencia() {
        return existencia;
    }

    /**
     * @param existencia the existencia to set
     */
    public void setExistencia(boolean existencia) {
        this.existencia = existencia;
    }
    
    
}//fin de la clase
