package servidor_carrito;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import static servidor_carrito.Producto.imprimirProductos;

/**
 * @author thrashformer
 */
public class Main extends javax.swing.JFrame {
    //insertamos las variables a usar 

        //String nombre, int cantidad, double precio 
        //new Servidor_ftp().iniciarServidor();
        String nombre_archivo= "productos.txt";
        //System.out.println("CREAREMOS OBJETOS");
        //Producto p1 = new Producto("Motocicleta 150cc", 20, 1200.50);
        //Producto p2 = new Producto();
        //Producto p3 = new Producto("PlayStation 3 ", 3, 6500.0);
        //System.out.println("GAURDAREMOS LOS OBJETOS EN UN ARCHIVO");
        //System.out.println("SE GUARDARON");

        //System.out.println("IMPRIMIREMOS");

        //imprimirProductos(productos);
        //System.out.println("SE IMPRIMIERON");

    Factory fabrica= new Factory();
    ArrayList<Producto> pdcts= new ArrayList<Producto>();
    
    public Main() 
    {
        initComponents();
        pdcts= fabrica.getProductos() ;
        //fabrica.crearProducto("Motocicleta 150cc", 20, 1200.50);
        /*
        fabrica.crearProducto("Carro Chevi", 3, 23000.50);
        fabrica.crearProducto("Doritos flaming Hot ", 100, 99.99);        
        fabrica.crearProducto("Raspberry pi 2", 40, 1200.11);
        fabrica.crearProducto("Refrigerador gigante", 900, 45523.20);
        fabrica.crearProducto("Tablet 9 pulgadas", 8, 12345.08);                
        fabrica.guardarProductos_Archivo(pdcts);
        */        
        //imprimirProductos(pdcts);
        
    }
    @SuppressWarnings("unchecked")


    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jButton1.setText("Iniciar Servidor");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(48, 48, 48)
                .addComponent(jButton1)
                .addContainerGap(27, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(52, 52, 52)
                .addComponent(jButton1)
                .addContainerGap(56, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        try {
            // TODO add your handling code here:
            new Servidor_ftp().iniciarServidor(pdcts);
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() 
        {
            public void run() 
            {
                new Main().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    // End of variables declaration//GEN-END:variables
}
