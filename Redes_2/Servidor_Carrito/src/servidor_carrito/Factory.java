package servidor_carrito;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import servidor_carrito.Producto;
/**
 *
 * @author Usuario
 */
public class Factory 
{//INICIO CLASE FACTORY
    
        ArrayList<Producto> products;

    public Factory()
    {     
  //      ps.add(new Persona());
     products= new ArrayList <>();    
  try 
  {
      FileInputStream fileC = new FileInputStream ("productos.txt");
      ObjectInputStream input = new ObjectInputStream (fileC);       

     products = (ArrayList <Producto>)input.readObject();     
      fileC.close();
      input.close();   
        
  } catch (Exception e)       
  { 
  System.out.println("Error Al Leer Productos");
  products.add(new Producto());
  try 
  {
      FileOutputStream file = new FileOutputStream ("productos.txt");
      ObjectOutputStream out = new ObjectOutputStream (file);       
      out.writeObject(products);
      file.close();
      out.close();      
  } catch (Exception d)  { System.out.println("Error Al Escribir Producto"); }
  
  
  }//Fin del Catch
  
}//Fin Constructor Factory
        
     public void crearProducto (String nombre, int cantidad, double precio){
        int id=products.size()+1;
         products.add(new Producto(id,nombre,cantidad,precio));
     }
     public void eliminarCliente(int id){
         for(int i=0; i<products.size();i++)
         {
             if(products.get(i).getId()==id)
                 products.remove(i);
         }
     }
     public ArrayList<Producto> getProductos(){
         return products;
     } 

     public boolean guardarProductos_Archivo(ArrayList<Producto> products)
      {
          try 
             {
                FileOutputStream file = new FileOutputStream ("productos.txt");
                ObjectOutputStream out = new ObjectOutputStream (file);       
                out.writeObject(products);
                file.close();
                out.close();      
                return true;
            } 
          catch (Exception d)  { System.out.println("Error Al Escribir Producto"); return false; }
          
      }
    
}