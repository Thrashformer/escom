import numpy as np
from cv2 import *
import cv2

def captura_color():
    try:
        namedWindow("webcam")
        vc = cv2.VideoCapture(0);
        while True:
            next, frame = vc.read()
            cv2.imshow("webcam", frame)
            if cv2.waitKey(50) >= 0:
                break

        # Se destruye la pantalla
        vc.release()
        cv2.destroyAllWindows()
    except Exception as e:
        print(e)


def captura_grises():
    cap = cv2.VideoCapture(0)
    capturas=True
    diez=10
    while(capturas):
        # Grabamos frame por frame
        ret, frame = cap.read()
        # 
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        # Se muestra la pantalla en escala de grises
        cv2.imshow('frame',gray)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

        diez-=1
        if diez==0:
            print('false')
            capturas=False

    # Se destruye la pantalla
    cap.release()
    cv2.destroyAllWindows()

captura_color()



