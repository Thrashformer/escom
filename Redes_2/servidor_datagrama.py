# -*- coding: utf-8 -*-
__author__ = 'thrashformer'

# Envio archivos: servidor
# 11Sep

import socket

# Creamos una lista con los datos del la conexión
CONEXION = (socket.gethostname(), 9001)
servidor = socket.socket()

# Ponemos el servidor a la escucha
servidor.bind(CONEXION)
servidor.listen(5)
print "Escuchando {0} en {1}".format(*CONEXION)
# Aceptamos conexiones
sck, addr = servidor.accept()
print "Conectado a: {0}:{1}".format(*addr)
while True:
    # Recibimos la longitud que envia el cliente
    tamanio_archivo = sck.recv(1024).strip()
    if tamanio_archivo:
        print('El archivo pesa ..%s bytes'%tamanio_archivo)
    # Verificamos que lo que recibimos sea un número
    # en caso que así sea, enviamos el mensaje "OK"
    # al cliente indicandole que estamos listos
    # para recibir el archivo
    if tamanio_archivo.isdigit():
        sck.send("OK")

        # Inicializamos el contador que
        # guardara la cantidad de bytes recibidos
        buffer = 0
        # Abrimos el archivo en modo escritura binaria
        with open("archivo", "wb") as archivo:

            # Nos preparamos para recibir el archivo
            # con la longitud específica
            while (buffer <= int(tamanio_archivo)):
                data = sck.recv(1)
                if not len(data):
                    # Si no recibimos datos
                    # salimos del bucle
                    break
                # Escribimos cada byte en el archivo
                # y aumentamos en uno el buffer
                archivo.write(data)
                buffer += 1

            if buffer == int(tamanio_archivo):
                print "Archivo descargado con éxito"
            else:
                print "Ocurrió un error/Archivo incompleto"
        break


'''
from cv2 import *


DEF ELEGIR_ARCHIVO():
    from tkFileDialog import *
    ruta_del_directorio=askdirectory()
    archivo=askopenfile()
    print (type(ruta_del_directorio),ruta_del_directorio)


def func():
    try:
        namedWindow("webcam")
        vc = VideoCapture(0);
        while True:
            next, frame = vc.read()
            print('x, next',next)
            imshow("webcam", frame)
            print('x')
            if waitKey(50) >= 0:
                break;
    except Exception as e:
        print(e)


    #
    pass

#func()

'''


'''
import pygame
import Tkinter as tk
from Tkinter import *
import os

pygame.init()
print (pygame.display.list_modes())
root = tk.Tk()
embed = tk.Frame(root, width = 500, height = 500) #creates embed frame for pygame window
embed.grid(columnspan = (600), rowspan = 500) # Adds grid
embed.pack(side = LEFT) #packs window to the left
buttonwin = tk.Frame(root, width = 75, height = 500)
buttonwin.pack(side = LEFT)
os.environ['SDL_WINDOWID'] = str(embed.winfo_id())
os.environ['SDL_VIDEODRIVER'] = 'windib'

screen = pygame.display.set_mode((500,500))
screen.fill(pygame.Color(255,255,255))
pygame.display.init()
pygame.display.update()
def draw():
    pygame.draw.circle(screen, (0,0,0), (250,250), 125)
    pygame.display.update()
    button1 = Button(buttonwin,text = 'Draw',  command=draw)
    button1.pack(side=LEFT)
    root.update()

while True:
    pygame.display.update()
    root.update()
#########################3
'''