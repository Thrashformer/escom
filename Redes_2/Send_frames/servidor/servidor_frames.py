#!/usr/bin/python

__author__ = 'metallica'

import random, pygame, sys , numpy
from pygame.locals import *
import socket
import cv2

#          VARIABLES GLOBALES

TCP_IP = 'localhost'
#TCP_IP= '10.10.1.189'
#TCP_IP = '192.168.1.73'
TCP_PORT = 5001


#   VARIABLES  PARA PYGAME
FPS = 3
ROJO = (255,0,0)
BLANCO = (255, 255, 255)
NEGRO = (0,0,0)
AZUL =(0,0,220)
TRANSPARENT = (255,0,255)
FONDO = NEGRO
pygame.init()
#Ancho de la vetana
WINDOWWIDTH = 600
#Alto de la Ventana
WINDOWHEIGHT = 600

def recvall(sock, count):
    buf = b''
    while count:
        newbuf = sock.recv(count)
        if not newbuf: return None
        buf += newbuf
        count -= len(newbuf)
    return buf



def cvimage_to_pygame(image):
    """Convert cvimage into a pygame image"""
    return pygame.image.frombuffer(image.tostring(), image.shape[1::-1] ,"RGB")


# recibe imagenes, captura eventos con pygame
def recibir_imagen2():
    #inicia pygame y su pantalla
    #pygame.init()
    #screen = pygame.display.set_mode((468, 60))
    #pygame.display.set_caption('Capturando video hacia pygame')
    #pygame.mouse.set_visible(0)
    #print('termina de cargar pygame')

    #print('Path to pygame module:', pygame.__file__)
    #Inicia conexion
    print('Inicio servidor en %s...'%TCP_IP)
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((TCP_IP, TCP_PORT))
    s.listen(True)
    conn, addr = s.accept()
    print('Se conecto...')

    #ya conecto
    while (True):
        #Calcula cuanto mide/pesa el archivo recibido
        #print('bucle, (conn y addr)',conn,addr)
        length = recvall(conn,16)

        stringData = recvall(conn, int(length))
        data = numpy.fromstring(stringData, dtype='uint8')
        #print('data ',data)

        decimg=cv2.imdecode(data,1)
        cv2.imshow('SERVER',decimg)
        cv2.waitKey(10)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            print('apreto q, podemos meter un evento aqui :D')


        #print('Path to pygame module:', pygame.__file__)
        #if event.type == QUIT:
        #    return
        #elif event.type == KEYDOWN and event.key == K_TAB:
        #    print ('apreto la tecla espacio')

    #termino la conexion
    s.close()

    #decimg=cv2.imdecode(data,1)
    #cv2.imshow('SERVER',decimg)
    #cv2.waitKey(10)
    cv2.destroyAllWindows()

recibir_imagen2()

