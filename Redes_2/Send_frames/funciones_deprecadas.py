__author__ = 'metallica'

##
##        FUNCIONES EN CLIENTE
##
'''
#!/usr/bin/python
import socket
import cv2
import numpy

TCP_IP = 'localhost'
TCP_PORT = 5001

sock = socket.socket()
sock.connect((TCP_IP, TCP_PORT))

capture = cv2.VideoCapture(0)
ret, frame = capture.read()

encode_param=[int(cv2.IMWRITE_JPEG_QUALITY),90]
result, imgencode = cv2.imencode('.jpg', frame, encode_param)
data = numpy.array(imgencode)
stringData = data.tostring()

sock.send( str(len(stringData)).ljust(16));
sock.send( stringData );
sock.close()

decimg=cv2.imdecode(data,1)
cv2.imshow('CLIENT',decimg)
cv2.waitKey(0)
cv2.destroyAllWindows()

'''

##
##      SERVIDOR
##

'''
#!/usr/bin/python
import socket
import cv2
import numpy

def recvall(sock, count):
    buf = b''
    while count:
        newbuf = sock.recv(count)
        if not newbuf: return None
        buf += newbuf
        count -= len(newbuf)
    return buf

TCP_IP = 'localhost'
TCP_PORT = 5001

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((TCP_IP, TCP_PORT))
s.listen(True)
conn, addr = s.accept()

length = recvall(conn,16)
stringData = recvall(conn, int(length))
data = numpy.fromstring(stringData, dtype='uint8')
s.close()

decimg=cv2.imdecode(data,1)
cv2.imshow('SERVER',decimg)
cv2.waitKey(0)
cv2.destroyAllWindows()

'''


# recibe imagenes
def recibir_imagen():
    #Inicia conexion
    print('Inicio servidor en %s...'%TCP_IP)
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((TCP_IP, TCP_PORT))
    s.listen(True)
    conn, addr = s.accept()
    print('Se conecto...')

    #ya conecto

    while (True):
        #Calcula cuanto mide/pesa el archivo recibido
        #print('bucle, (conn y addr)',conn,addr)
        length = recvall(conn,16)

        stringData = recvall(conn, int(length))
        data = numpy.fromstring(stringData, dtype='uint8')
        #print('data ',data)

        decimg=cv2.imdecode(data,1)
        cv2.imshow('SERVER',decimg)
        cv2.waitKey(10)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            print('apreto q, podemos meter un evento aqui :D')


    #termino la conexion
    s.close()

    #decimg=cv2.imdecode(data,1)
    #cv2.imshow('SERVER',decimg)
    #cv2.waitKey(10)
    cv2.destroyAllWindows()


## funciones que no son de alguno en especifico



def main():
    print "Inicio..."
    global FPSCLOCK, pantalla
    FPSCLOCK = pygame.time.Clock()
    '''inicializamos el mundo '''
    pantalla  = pygame.display.set_mode((WINDOWWIDTH, WINDOWHEIGHT))
    '''color de fondo del mundo'''
    #pantalla.fill(FONDO)
    pantalla.fill(BLANCO)
    pantalla.set_colorkey(TRANSPARENT)
    pantalla.set_alpha(160)
    pantalla_string  = pygame.image.tostring(pantalla,"RGBA")



    nombre_img = "wings"
    imagen_original = pygame.image.load(nombre_img+".png")

    poblacion = [chr(255) for i in range (1200) ]
    #   dibuja en pantalla
    pantalla.blit( pygame.image.frombuffer(poblacion[0], tam_imagen ,"RGBA"), (0,0))

    juego = True
    iteracion = 0
    while juego:
        for event in pygame.event.get(): # event handling loop
            if event.type == QUIT or (event.type == KEYUP and event.key == K_ESCAPE):
                pygame.quit()
                sys.exit()
            if (event.type == KEYUP and event.key == K_f):
                ''' guardar el surface a imagen PNG'''
                pygame.image.save(pantalla , "capturas/prueba_strings.png")

        '''AL FINAL ACTUALIZAMOS LA PANTALLA '''
        #Actualizamos la pantalla
        #pygame.display.flip()
        pygame.display.update()
        FPSCLOCK.tick(FPS)
        #juego = False
        #print "termino"
        #input()
        iteracion+=1
    pygame.quit()
