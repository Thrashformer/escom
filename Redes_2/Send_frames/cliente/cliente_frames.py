#!/usr/bin/python
__author__ = 'metallica'

import socket
import cv2
import numpy

TCP_IP = 'localhost'
#TCP_IP = '192.168.1.73'
TCP_PORT = 5001


def captura(capture):
    #capture = cv2.VideoCapture(0)
    ret, frame = capture.read()

    encode_param=[int(cv2.IMWRITE_JPEG_QUALITY),90]
    result, imgencode = cv2.imencode('.jpg', frame, encode_param)
    data = numpy.array(imgencode)
    stringData = data.tostring()
    return data, stringData

# una sola funcion que conecta y envia las imagenes
def enviar_capturas():
    sock = socket.socket()
    sock.connect((TCP_IP, TCP_PORT))
    termina_conexion=False
    capturas= 2000
    while not termina_conexion:
        capture = cv2.VideoCapture(0)
        ret, frame = capture.read()

        encode_param=[int(cv2.IMWRITE_JPEG_QUALITY),90]
        result, imgencode = cv2.imencode('.jpg', frame, encode_param)
        data = numpy.array(imgencode)
        stringData = data.tostring()

        decimg=cv2.imdecode(data,1)

        cv2.imshow('CLIENT',decimg)

        if capturas is 0:
            termina_conexion=True

    #termina la conexion
    sock.close()

    cv2.waitKey(10)
    cv2.destroyAllWindows()


#Cliente envia la imagen
def send_imagen():

    sock = socket.socket()
    sock.connect((TCP_IP, TCP_PORT))
    print('conectados a %s'%TCP_IP)
    #dejamos la camara activa
    capture = cv2.VideoCapture(0)

    termina_conexion=False
    #capturas= 2000
    while not termina_conexion:
        data, stringData = captura(capture)
        sock.send( str(len(stringData)).ljust(16))
        #print('stringData',len(stringData))
        sock.send( stringData )
        #print('enviado')
        decimg=cv2.imdecode(data,1)
        #cv2.imshow('CLIENT',decimg)
        #cv2.waitKey(10)
        if cv2.waitKey(1) & 0xFF == ord('r'):
            print('apreto r, podemos meter un evento aqui :D')


        #capturas-=1
        #if capturas is 0:
        #    termina_conexion=True

    #termina la conexion
    sock.close()

    cv2.waitKey(10)
    cv2.destroyAllWindows()

#enviar_capturas()
send_imagen()



#Cliente envia la imagen, pygame para capturar eventos
def send_imagen():
    sock = socket.socket()
    sock.connect((TCP_IP, TCP_PORT))
    print('conectados a %s'%TCP_IP)
    #dejamos la camara activa
    capture = cv2.VideoCapture(0)
    termina_conexion=False
    #capturas= 2000
    while not termina_conexion:
        data, stringData = captura(capture)
        sock.send( str(len(stringData)).ljust(16))
        #print('stringData',len(stringData))
        sock.send( stringData )
        #print('enviado')
        decimg=cv2.imdecode(data,1)
        #cv2.imshow('CLIENT',decimg)
        #cv2.waitKey(10)
        if cv2.waitKey(1) & 0xFF == ord('r'):
            print('apreto r, podemos meter un evento aqui :D')


        #capturas-=1
        #if capturas is 0:
        #    termina_conexion=True

    #termina la conexion
    sock.close()

    cv2.waitKey(10)
    cv2.destroyAllWindows()


