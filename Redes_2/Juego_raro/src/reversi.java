import java.io.BufferedReader;

import java.io.IOException;

import java.io.InputStreamReader;

 

public class reversi {

    static boolean inv = false;

    static int flipcount = 0;

    static int flips = 0;

    static String pone = "";

    static String ptwo = "";

    static String twogo = "";

    static String onego = "";

    static String f = "";

    static boolean start = true;

    static int turn = 0;

    static String one[] = new String[8];

    static String two[] = new String[8];

    static String three[] = new String[8];

    static String four[] = new String[8];

    static String five[] = new String[8];

    static String six[] = new String[8];

    static String seven[] = new String[8];

    static String eight[] = new String[8];

    static String onet[] = new String[8];

    static String twot[] = new String[8];

    static String threet[] = new String[8];

    static String fourt[] = new String[8];

    static String fivet[] = new String[8];

    static String sixt[] = new String[8];

    static String sevent[] = new String[8];

    static String eightt[] = new String[8];

    static int check[] = new int[100];

    static int xco = 0;

    static int yco = 0;

    static int turnt;

    static int counter = 0;

    static int[] direction = new int[8];

    static int[] boarder = { 10, -10, 1, -1, +11, -11, -9, +9 };

    static String blnk = "    ";

    static String blnk2 = "____";

    static String xtop = "\\__/";

    static String xbottom = "/  \\";

    static String o = "OOOO";

    static int tmp;

    static int y;

    static int z;

    static int h = 0;

 

    public static void main(String[] args) {

        start();

    }

 

    public static void start() {

        boolean c = true;

        int total = 0;

        int exes = 0;

        int ys = 0;

        for (int x = 0; x < 100; x++){

            if ((check[x] == 1) || (check[x] == 2) || (check[x] == 3)){

                total++;

            }

            if (check[x] == 1){

                exes++;

            }

            if (check[x] == 2){

                ys++;

            }

        }

        if (total == 100){

            System.out.println("Juego Finalizado! "+pone+" has "+exes+" cruces en la pizarra!\n"+ptwo+" has "+ys+" circulos en la pizarra!");

            if (ys < exes){

                f = readLine("Felicidades "+pone+"! Has ganado!\nTe gustaria jugar otra vez? ");

            } else if (ys > exes){

                f = readLine("Felicidades "+ptwo+"! Has ganado!\nTe gustaria jugar otra vez? ");

            } else if (ys == exes){

                f = readLine("Felicidades a ambos jugadores! has ganado!\nTe gustaria jugar otra vez? ");

            }

            do {

                if (f.equals("yes") || f.equals("y") || f.equals("YES")){

                    start = true;

                    start();

                } else if (f.equals("no") || f.equals("n") || f.equals("NO")){

                    System.out.println("Espero que hayas disfrutado Othello (Reversi) por Greg Cawthorne!");

                    System.exit(0);

                } else {

                    f = readLine("Lo siento, no entendi bien. Te gustaria jugar otra vez? ");

                    c = false;

                }

            } while (c == false);

        } else if ((start == false) && (turn != 0) && (inv == false)){

                System.out.print(pone+" has "+exes+" (X) recuadros!\n"+ptwo+" has "+ys+" (0) recuadros!\n");

        }

        inv = false;

        boolean chin = true;

        if (start == true) {

            pone = readLine("Bienvenido a Othello (Reversi) por Greg Cawthorne!\nPor favor, ingrese el nombre de un jugador! ");

            while (pone.equals("")) {

                pone = readLine("Por favor, entra el nombre del primer jugador :) ");

            }

            ptwo = readLine("El nombre del primer jugador es " + pone

                + ".\nPor favor, ingrese el nombre de dos jugadores! ");

            while (ptwo.equals("")) {

                ptwo = readLine("Por favor, entra el nombre del segundo jugador :) ");

            }

            f = readLine("El nombre del segundo jugador es "

                + ptwo

                + "\nSólo tiene que pulsar Intro para comenzar o presiona alguna tecla para cambiar los nombres de los jugadores: ");

            if (false == f.equals("")) {

                start();

            }

        }

        for (int x = 0; x < 100; x++) {

            check[x] = 3;

            if (x == 9) {

                x = 89;

            }

        }

        for (int x = 0; x < 100; x = x + 10) {

            check[x] = 3;

            if (x == 90) {

                x = 9;

            }

        }

        if (start == true) {

            for (int x = 0; x < 8; x++) {

                one[x] = blnk;

                two[x] = blnk;

                three[x] = blnk;

                five[x] = blnk;

                six[x] = blnk;

                seven[x] = blnk;

                eight[x] = blnk;

                onet[x] = blnk2;

                twot[x] = blnk2;

                threet[x] = blnk2;

                fivet[x] = blnk2;

                sixt[x] = blnk2;

                sevent[x] = blnk2;

                eightt[x] = blnk2;

                check[44] = 2;

                check[45] = 1;

                check[54] = 1;

                check[55] = 2;

                if (x == 3) {

                    five[x] = xtop;

                    fivet[x] = xbottom;

                    four[x] = o;

                    fourt[x] = o;

                } else if (x == 4) {

                    five[x] = o;

                    fivet[x] = o;

                    four[x] = xtop;

                    fourt[x] = xbottom;

                } else {

                    five[x] = blnk;

                    fivet[x] = blnk2;

                    four[x] = blnk;

                    fourt[x] = blnk2;

                }

            }// end for

 

        } else if (turn % 2 == 0) {

            turnt = 2;

            counter = 1;

            while (chin == true) {

 

                onego = readLine(pone

                    + ", Por favor, introduzca las coordenadas del lugar donde desea colocar una (X) (e.g. x,y) ");

                while ((false == onego.equals("1,1"))

                    && (false == onego.equals("1,2"))

                    && (false == onego.equals("1,3"))

                    && (false == onego.equals("1,4"))

                    && (false == onego.equals("1,5"))

                    && (false == onego.equals("1,6"))

                    && (false == onego.equals("1,7"))

                    && (false == onego.equals("1,8"))

                    && (false == onego.equals("2,1"))

                    && (false == onego.equals("2,2"))

                    && (false == onego.equals("2,3"))

                    && (false == onego.equals("2,4"))

                    && (false == onego.equals("2,5"))

                    && (false == onego.equals("2,6"))

                    && (false == onego.equals("2,7"))

                    && (false == onego.equals("2,8"))

                    && (false == onego.equals("3,1"))

                    && (false == onego.equals("3,2"))

                    && (false == onego.equals("3,3"))

                    && (false == onego.equals("3,4"))

                    && (false == onego.equals("3,5"))

                    && (false == onego.equals("3,6"))

                    && (false == onego.equals("3,7"))

                    && (false == onego.equals("3,8"))

                    && (false == onego.equals("4,1"))

                    && (false == onego.equals("4,2"))

                    && (false == onego.equals("4,3"))

                    && (false == onego.equals("4,4"))

                    && (false == onego.equals("4,5"))

                    && (false == onego.equals("4,6"))

                    && (false == onego.equals("4,7"))

                    && (false == onego.equals("4,8"))

                    && (false == onego.equals("5,1"))

                    && (false == onego.equals("5,2"))

                    && (false == onego.equals("5,3"))

                    && (false == onego.equals("5,4"))

                    && (false == onego.equals("5,5"))

                    && (false == onego.equals("5,6"))

                    && (false == onego.equals("5,7"))

                    && (false == onego.equals("5,8"))

                    && (false == onego.equals("6,1"))

                    && (false == onego.equals("6,2"))

                    && (false == onego.equals("6,3"))

                    && (false == onego.equals("6,4"))

                    && (false == onego.equals("6,5"))

                    && (false == onego.equals("6,6"))

                    && (false == onego.equals("6,7"))

                    && (false == onego.equals("6,8"))

                    && (false == onego.equals("7,1"))

                    && (false == onego.equals("7,2"))

                    && (false == onego.equals("7,3"))

                    && (false == onego.equals("7,4"))

                    && (false == onego.equals("7,5"))

                    && (false == onego.equals("7,6"))

                    && (false == onego.equals("7,7"))

                    && (false == onego.equals("7,8"))

                    && (false == onego.equals("8,1"))

                    && (false == onego.equals("8,2"))

                    && (false == onego.equals("8,3"))

                    && (false == onego.equals("8,4"))

                    && (false == onego.equals("8,5"))

                    && (false == onego.equals("8,6"))

                    && (false == onego.equals("8,7"))

                    && (false == onego.equals("8,8"))) {

                    onego = readLine("Posición incorrecta. "

                        + pone

                        + ", Por favor, introduzca las coordenadas del lugar donde desea colocar una (X) (e.g. x,y) ");

                }

                chin = false;

                String x = String.valueOf(onego.charAt(0));

                String y = String.valueOf(onego.charAt(2));

                xco = Integer.parseInt(x);

                yco = Integer.parseInt(y);

                tmp = check[xco + yco * 10];

                if ((yco == 1) && (check[xco + 10] == 0)) {

                    one[xco - 1] = xtop;

                    onet[xco - 1] = xbottom;

                    check[xco + 10] = 1;

                } else if ((yco == 2) && (check[xco + 20] == 0)) {

                    two[xco - 1] = xtop;

                    twot[xco - 1] = xbottom;

                    check[xco + 20] = 1;

                } else if ((yco == 3) && (check[xco + 30] == 0)) {

                    three[xco - 1] = xtop;

                    threet[xco - 1] = xbottom;

                    check[xco + 30] = 1;

                } else if ((yco == 4) && (check[xco + 40] == 0)) {

                    four[xco - 1] = xtop;

                    fourt[xco - 1] = xbottom;

                    check[xco + 40] = 1;

                } else if ((yco == 5) && (check[xco + 50] == 0)) {

                    five[xco - 1] = xtop;

                    fivet[xco - 1] = xbottom;

                    check[xco + 50] = 1;

                } else if ((yco == 6) && (check[xco + 60] == 0)) {

                    six[xco - 1] = xtop;

                    sixt[xco - 1] = xbottom;

                    check[xco + 60] = 1;

                } else if ((yco == 7) && (check[xco + 70] == 0)) {

                    seven[xco - 1] = xtop;

                    sevent[xco - 1] = xbottom;

                    check[xco + 70] = 1;

                } else if ((yco == 8) && (check[xco + 80] == 0)) {

                    eight[xco - 1] = xtop;

                    eightt[xco - 1] = xbottom;

                    check[xco + 80] = 1;

                } else {

                    chin = true;

                    System.out.print("Esta coordenada ya está llena. ");

                }// end else

            }

        } else {

            while (chin == true) {

                turnt = 1;

                counter = 2;

                twogo = readLine(ptwo

                                + ", Por favor, introduzca las coordenadas del lugar donde desea colocar una (O) (e.g. x,y) ");

                while ((false == twogo.equals("1,1"))

                    && (false == twogo.equals("1,2"))

                    && (false == twogo.equals("1,3"))

                    && (false == twogo.equals("1,4"))

                    && (false == twogo.equals("1,5"))

                    && (false == twogo.equals("1,6"))

                    && (false == twogo.equals("1,7"))

                    && (false == twogo.equals("1,8"))

                    && (false == twogo.equals("2,1"))

                    && (false == twogo.equals("2,2"))

                    && (false == twogo.equals("2,3"))

                    && (false == twogo.equals("2,4"))

                    && (false == twogo.equals("2,5"))

                    && (false == twogo.equals("2,6"))

                    && (false == twogo.equals("2,7"))

                    && (false == twogo.equals("2,8"))

                    && (false == twogo.equals("3,1"))

                    && (false == twogo.equals("3,2"))

                    && (false == twogo.equals("3,3"))

                    && (false == twogo.equals("3,4"))

                    && (false == twogo.equals("3,5"))

                    && (false == twogo.equals("3,6"))

                    && (false == twogo.equals("3,7"))

                    && (false == twogo.equals("3,8"))

                    && (false == twogo.equals("4,1"))

                    && (false == twogo.equals("4,2"))

                    && (false == twogo.equals("4,3"))

                    && (false == twogo.equals("4,4"))

                    && (false == twogo.equals("4,5"))

                    && (false == twogo.equals("4,6"))

                    && (false == twogo.equals("4,7"))

                    && (false == twogo.equals("4,8"))

                    && (false == twogo.equals("5,1"))

                    && (false == twogo.equals("5,2"))

                    && (false == twogo.equals("5,3"))

                    && (false == twogo.equals("5,4"))

                    && (false == twogo.equals("5,5"))

                    && (false == twogo.equals("5,6"))

                    && (false == twogo.equals("5,7"))

                    && (false == twogo.equals("5,8"))

                    && (false == twogo.equals("6,1"))

                    && (false == twogo.equals("6,2"))

                    && (false == twogo.equals("6,3"))

                    && (false == twogo.equals("6,4"))

                    && (false == twogo.equals("6,5"))

                    && (false == twogo.equals("6,6"))

                    && (false == twogo.equals("6,7"))

                    && (false == twogo.equals("6,8"))

                    && (false == twogo.equals("7,1"))

                    && (false == twogo.equals("7,2"))

                    && (false == twogo.equals("7,3"))

                    && (false == twogo.equals("7,4"))

                    && (false == twogo.equals("7,5"))

                    && (false == twogo.equals("7,6"))

                    && (false == twogo.equals("7,7"))

                    && (false == twogo.equals("7,8"))

                    && (false == twogo.equals("8,1"))

                    && (false == twogo.equals("8,2"))

                    && (false == twogo.equals("8,3"))

                    && (false == twogo.equals("8,4"))

                    && (false == twogo.equals("8,5"))

                    && (false == twogo.equals("8,6"))

                    && (false == twogo.equals("8,7"))

                    && (false == twogo.equals("8,8"))) {

                    twogo = readLine("Posición incorrecta. "

                        + ptwo

                        + ", Por favor, introduzca las coordenadas del lugar donde desea colocar una (O) (e.g. x,y) ");

                }

                chin = false;

                String x = String.valueOf(twogo.charAt(0));

                String y = String.valueOf(twogo.charAt(2));

                xco = Integer.parseInt(x);

                yco = Integer.parseInt(y);

                tmp = check[xco + yco * 10];

                if ((yco == 1) && (check[xco + 10] == 0)) {

                    one[xco - 1] = o;

                    onet[xco - 1] = o;

                    check[xco + 10] = 2;

                } else if ((yco == 2) && (check[xco + 20] == 0)) {

                    two[xco - 1] = o;

                    twot[xco - 1] = o;

                    check[xco + 20] = 2;

                } else if ((yco == 3) && (check[xco + 30] == 0)) {

                    three[xco - 1] = o;

                    threet[xco - 1] = o;

                    check[xco + 30] = 2;

                } else if ((yco == 4) && (check[xco + 40] == 0)) {

                    four[xco - 1] = o;

                    fourt[xco - 1] = o;

                    check[xco + 40] = 2;

                } else if ((yco == 5) && (check[xco + 50] == 0)) {

                    five[xco - 1] = o;

                    fivet[xco - 1] = o;

                    check[xco + 50] = 2;

                } else if ((yco == 6) && (check[xco + 60] == 0)) {

                    six[xco - 1] = o;

                    sixt[xco - 1] = o;

                    check[xco + 60] = 2;

                } else if ((yco == 7) && (check[xco + 70] == 0)) {

                    seven[xco - 1] = o;

                    sevent[xco - 1] = o;

                    check[xco + 70] = 2;

                } else if ((yco == 8) && (check[xco + 80] == 0)) {

                    eight[xco - 1] = o;

                    eightt[xco - 1] = o;

                    check[xco + 80] = 2;

                } else {

                    chin = true;

                    System.out.print("Esta coordenada ya está llena. ");

                }

            }

        }// end else

        if (start == true) {

            board(one, two, three, four, five, six, seven, eight, onet, twot,

                            threet, fourt, fivet, sixt, sevent, eightt);

            start = false;

            start();

        }

        check();

        board(one, two, three, four, five, six, seven, eight, onet, twot,

            threet, fourt, fivet, sixt, sevent, eightt);

    }

 

    public static void check() {

        for (int x = 0; x < 8 ; x++){

            direction[x] = 0;

        }

        int p = 0;

        if (check[xco + (yco * 10)] == counter) {

            for (int x = 0; x < 8; x++) {

                if (check[xco + (yco * 10) + boarder[x]] == turnt) {

                    direction[x] = boarder[x];

                }

            }

        }

        for (int x = 0; x < 8; x++) {

            if (direction[x] == 0) {

                p++;

            }

        }

        if (p == 8) {

            invalidturn();

        } else {

            extra();

        }

    }

 

    public static void invalidturn() {

        String pic = null;

        String pic2 = null;

        System.out.print("Este no es un turno valido ");

        check[xco + yco * 10] = tmp;

        if (tmp == 1){

            pic = xtop;

            pic2 = xbottom;

        } else if (tmp == 2){

            pic = o;

            pic2 = o;

        } else if (tmp == 0){

            pic = blnk;

            pic2 = blnk2;

        }

        if ((yco == 1)) {

            one[xco - 1] = pic;

            onet[xco - 1] = pic2;

        } else if ((yco == 2)) {

            two[xco - 1] = pic;

            twot[xco - 1] = pic2;

        } else if ((yco == 3)) {

            three[xco - 1] = pic;

            threet[xco - 1] = pic2;

        } else if ((yco == 4)) {

            four[xco - 1] = pic;

            fourt[xco - 1] = pic2;

        } else if ((yco == 5)) {

            five[xco - 1] = pic;

            fivet[xco - 1] = pic2;

        } else if ((yco == 6)) {

            six[xco - 1] = pic;

            sixt[xco - 1] = pic2;

        } else if ((yco == 7)) {

            seven[xco - 1] = pic;

            sevent[xco - 1] = pic2;

        } else if ((yco == 8)) {

            eight[xco - 1] = pic;

            eightt[xco - 1] = pic2;

        }

        inv = true;

        start();

 

    }

 

    public static void extra() {

        y = 0;

        z = 2;

        boolean end = false;

        for (h = h + 0; h <= 8; h++) {

            if (h == 8) {

                if (flipcount == 0){

                    flipcount = 0;

                    h = 0;

                    invalidturn();

                }

                flipcount = 0;

                h = 0;

                board(one, two, three, four, five, six, seven, eight, onet, twot,

                    threet, fourt, fivet, sixt, sevent, eightt);

            }

            if (direction[h] != 0) {

                for (y = 0; y < 8; y++) {

                    if (direction[h] == boarder[y]) {

                        if ((check[xco + (yco * 10) + boarder[y]] == turnt)) {

                            while (end == false) {

                                if ((check[xco + (yco * 10) + (boarder[y]) * z] == turnt)) {

                                    z++;

                                } else if ((check[xco + (yco * 10)

                                            + (boarder[y]) * z] == counter)) {

                                    flipcount++;

                                    h++;

                                    end = true;

                                    flip(z);

                                } else if ((check[xco + (yco * 10)

                                        + (boarder[y]) * z] == 3)

                                        || (check[xco + (yco * 10)

                                        + (boarder[y]) * z] == 0)) {

                                    h++;

                                    extra();

                                }

                            }

                        }

                    }

                }

            }

        }

    }

 

    public static void flip(int z) {

        String pic = null;

        String pic2 = null;

        int tmpy = yco;

        int tmpx = xco;

        if (counter == 2) {

            pic = o;

            pic2 = o;

        } else if (counter == 1) {

            pic = xtop;

            pic2 = xbottom;

        }

        for (int x = 0; x < z; x++) {

            check[xco + yco * 10 + boarder[y] * x] = counter;

 

            if (boarder[y] == 10) {

                tmpy = tmpy + 1;

            } else if (boarder[y] == -10) {

                tmpy = tmpy - 1;

            } else if (boarder[y] == 1) {

                tmpx = tmpx + 1;

            } else if (boarder[y] == -1) {

                tmpx = tmpx - 1;

            } else if (boarder[y] == 11) {

                tmpy = tmpy + 1;

                tmpx = tmpx + 1;

            } else if (boarder[y] == -11) {

                tmpy = tmpy - 1;

                tmpx = tmpx - 1;

            } else if (boarder[y] == 9) {

                tmpy = tmpy + 1;

                tmpx = tmpx - 1;

            } else if (boarder[y] == -9) {

                tmpy = tmpy - 1;

                tmpx = tmpx + 1;

            }

 

            if ((tmpy == 1)) {

                one[tmpx - 1] = pic;

                onet[tmpx - 1] = pic2;

            } else if ((tmpy == 2)) {

                two[tmpx - 1] = pic;

                twot[tmpx - 1] = pic2;

            } else if ((tmpy == 3)) {

                three[tmpx - 1] = pic;

                threet[tmpx - 1] = pic2;

            } else if ((tmpy == 4)) {

                four[tmpx - 1] = pic;

                fourt[tmpx - 1] = pic2;

            } else if ((tmpy == 5)) {

                five[tmpx - 1] = pic;

                fivet[tmpx - 1] = pic2;

            } else if ((tmpy == 6)) {

                six[tmpx - 1] = pic;

                sixt[tmpx - 1] = pic2;

            } else if ((tmpy == 7)) {

                seven[tmpx - 1] = pic;

                sevent[tmpx - 1] = pic2;

            } else if ((tmpy == 8)) {

                eight[tmpx - 1] = pic;

                eightt[tmpx - 1] = pic2;

            }

        }

        extra();

    }

 

    public static void board(String[] one, String[] two, String[] three,

                String[] four, String[] five, String[] six, String[] seven,

                String[] eight, String[] onet, String[] twot, String[] threet,

                String[] fourt, String[] fivet, String[] sixt, String[] sevent,

                String[] eightt) {

        System.out.println("._______________________________________");

        System.out.println("|" + eight[0] + "|" + eight[1] + "|" + eight[2]

                        + "|" + eight[3] + "|" + eight[4] + "|" + eight[5] + "|"

                        + eight[6] + "|" + eight[7] + "|");

        System.out.println("|" + eightt[0] + "|" + eightt[1] + "|" + eightt[2]

                        + "|" + eightt[3] + "|" + eightt[4] + "|" + eightt[5] + "|"

                        + eightt[6] + "|" + eightt[7] + "|8");

        System.out.println("|" + seven[0] + "|" + seven[1] + "|" + seven[2]

                        + "|" + seven[3] + "|" + seven[4] + "|" + seven[5] + "|"

                        + seven[6] + "|" + seven[7] + "|");

        System.out.println("|" + sevent[0] + "|" + sevent[1] + "|" + sevent[2]

                        + "|" + sevent[3] + "|" + sevent[4] + "|" + sevent[5] + "|"

                        + sevent[6] + "|" + sevent[7] + "|7");

        System.out.println("|" + six[0] + "|" + six[1] + "|" + six[2] + "|"

                        + six[3] + "|" + six[4] + "|" + six[5] + "|" + six[6] + "|"

                        + six[7] + "|");

        System.out.println("|" + sixt[0] + "|" + sixt[1] + "|" + sixt[2] + "|"

                        + sixt[3] + "|" + sixt[4] + "|" + sixt[5] + "|" + sixt[6] + "|"

                        + sixt[7] + "|6");

        System.out.println("|" + five[0] + "|" + five[1] + "|" + five[2] + "|"

                        + five[3] + "|" + five[4] + "|" + five[5] + "|" + five[6] + "|"

                        + five[7] + "|");

        System.out.println("|" + fivet[0] + "|" + fivet[1] + "|" + fivet[2]

                        + "|" + fivet[3] + "|" + fivet[4] + "|" + fivet[5] + "|"

                        + fivet[6] + "|" + fivet[7] + "|5");

        System.out.println("|" + four[0] + "|" + four[1] + "|" + four[2] + "|"

                        + four[3] + "|" + four[4] + "|" + four[5] + "|" + four[6] + "|"

                        + four[7] + "|");

        System.out.println("|" + fourt[0] + "|" + fourt[1] + "|" + fourt[2]

                        + "|" + fourt[3] + "|" + fourt[4] + "|" + fourt[5] + "|"

                        + fourt[6] + "|" + fourt[7] + "|4");

        System.out.println("|" + three[0] + "|" + three[1] + "|" + three[2]

                        + "|" + three[3] + "|" + three[4] + "|" + three[5] + "|"

                        + three[6] + "|" + three[7] + "|");

        System.out.println("|" + threet[0] + "|" + threet[1] + "|" + threet[2]

                        + "|" + threet[3] + "|" + threet[4] + "|" + threet[5] + "|"

                        + threet[6] + "|" + threet[7] + "|3");

        System.out.println("|" + two[0] + "|" + two[1] + "|" + two[2] + "|"

                        + two[3] + "|" + two[4] + "|" + two[5] + "|" + two[6] + "|"

                        + two[7] + "|");

        System.out.println("|" + twot[0] + "|" + twot[1] + "|" + twot[2] + "|"

                        + twot[3] + "|" + twot[4] + "|" + twot[5] + "|" + twot[6] + "|"

                        + twot[7] + "|2");

        System.out.println("|" + one[0] + "|" + one[1] + "|" + one[2] + "|"

                        + one[3] + "|" + one[4] + "|" + one[5] + "|" + one[6] + "|"

                        + one[7] + "|");

        System.out.println("|" + onet[0] + "|" + onet[1] + "|" + onet[2] + "|"

                        + onet[3] + "|" + onet[4] + "|" + onet[5] + "|" + onet[6] + "|"

                        + onet[7] + "|1");

        System.out.println("   1   2    3    4    5    6    7    8");

        if (start == false) {

            turn++;

            start();

        }

    }

 

    public static String readLine(String prompt) {

        String input = "";

        System.out.print(prompt);

        InputStreamReader isr = new InputStreamReader(System.in);

        BufferedReader br = new BufferedReader(isr);

        try {

 

            input = br.readLine();

 

        } catch (IOException ioe) {

        }

        return input;

    }

}