/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica3_hilos;

import controlador.Archivo;
import controlador.Hilo;
import controlador.Hilo_2;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class main_2 {

    public static void main(String[] args) {
        Archivo archivo = new Archivo("log_hilos.txt");
        int veces = 1000;
        ArrayList<Hilo> todos = new ArrayList<Hilo>();

        
        // /*
        Hilo hilo1 = new Hilo("Hilo_1", 1, archivo, veces); // fibonacci y veces a ejecutar
        Hilo hilo2 = new Hilo("Hilo_2", 0, archivo, veces); //random y veces a ejecutar
        Hilo hilo3 = new Hilo("Hilo_3", 4, archivo, veces); //INterrupciones  y veces a ejecutar

        todos.add(hilo1);
        todos.add(hilo2);
        hilo3.agregarHermanos(todos);
        try {
            hilo1.join();
            hilo2.join();
            hilo3.join();

        } catch (InterruptedException ex) {
            Logger.getLogger(main_2.class.getName()).log(Level.SEVERE, null, ex);
        }

        hilo1.start();
        hilo2.start();
        hilo3.start();
        archivo.cierra();
        //*/
        
         /*
        Hilo_2 hilo1 = new Hilo_2("Hilo_1", 1, archivo, veces); // fibonacci y veces a ejecutar
        Hilo_2 hilo2 = new Hilo_2("Hilo_2", 0, archivo, veces); //random y veces a ejecutar
        hilo1.start();
        hilo2.start();
        archivo.cierra();
        */

    
    }

}
