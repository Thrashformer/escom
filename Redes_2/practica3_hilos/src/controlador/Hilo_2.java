

package controlador;

import java.util.ArrayList;

public class Hilo_2 extends Thread 
{    
    int a = 0, b = 1, c = 0;
    int par = 0;
    int impar = 1;
    int pares = 0;
    int fibonacci = 1;
    int random = 3;
    int impares = 2;
    int interrupciones = 5;
    int vez;
    ArrayList<Hilo> hermanos = new ArrayList<Hilo>();

    Archivo archivo;

    // para detener el hilo
    private boolean continuar = true;
    // true= fibonacci , false=pares
    // true= random , false=impares
    private boolean rutina = true;

    private String nombre = "";
    private int accion = 0;

    public Hilo_2(String nombre, int accion, Archivo archivo, int vez) {
        this.nombre = nombre;
        this.accion = accion;
        this.archivo = archivo;
        this.vez = vez;
        System.out.println("soy "+this.nombre +" y tengo al archivo "+archivo.toString());
    }
    
    
    // Metodo del hilo
    public void run() {
        while (this.vez > 0) {
            switch (this.accion) {

                //random
                case 0:
                    this.archivo.println("ID : " + this.getId() + " Nombre : " + this.nombre + "; accion : Random: " + Math.random() * (101) + 1);
                    System.out.println("ID : " + this.getId() + " Nombre : " + this.nombre + "; accion : Random: " + Math.random() * (101) + 1);

                    //(int)(Math.random()*(HASTA-DESDE+1)+DESDE); 
                    break;

                //fibonacci
                case 1:
                    this.a = this.b;
                    this.b = this.c;
                    this.c = this.a + this.b;
                    this.archivo.println("ID : " + this.getId() + " Nombre : " + this.nombre + "; accion : FIBONACCI = " + this.c);
                    System.out.println("ID : " + this.getId() + " Nombre : " + this.nombre + "; accion : FIBONACCI = " + this.c);

                    break;
                //pares
                case 2:
                    this.par += 2;

                    this.archivo.println("ID : " + this.getId() + " Nombre : " + this.nombre + "; accion : PARES: " + this.par);
                    System.out.println("ID : " + this.getId() + " Nombre : " + this.nombre + "; accion : PARES: " + this.par);

                    break;

                //impares
                case 3:
                    this.impar += 2;
                    this.archivo.println("ID : " + this.getId() + " Nombre : " + this.nombre + "; accion : IMPAR: " + this.impar);
                    System.out.println("ID : " + this.getId() + " Nombre : " + this.nombre + "; accion : IMPAR: " + this.impar);

                    break;
                //interrupciones
                case 4:
                    this.archivo.println("ID : " + this.getId() + " Nombre : " + this.nombre + "; accion : INTERRUMPIR ");

                    try {
                        this.sleep(1000);
                    } catch (InterruptedException ex) {
                        System.out.println("\t\tInterupcion a hilo " + this.nombre);

                    }
                    for (Hilo hilo : hermanos) {
                        hilo.interrupt();

                    }

                    break;
            }//fin del swich
            this.vez--;
        }//fin del while    
    };    
}
