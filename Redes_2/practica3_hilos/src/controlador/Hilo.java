package controlador;

// Enumerado 
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

//  1=fibonacci
//  2=pares
//  3=random
//  4=impares
//  5=interrupciones
public class Hilo extends Thread {

    int a = 0, b = 1, c = 0;
    int par = 0;
    int impar = 1;
    int pares = 0;
    int fibonacci = 1;
    int random = 3;
    int impares = 2;
    int interrupciones = 5;
    int vez;
    ArrayList<Hilo> hermanos = new ArrayList<Hilo>();

    private Archivo archivo;

    // para detener el hilo
    private boolean continuar = true;
    // true= fibonacci , false=pares
    // true= random , false=impares
    private boolean rutina = true;

    private String nombre = "";
    private int accion = 0;

    //constructor
    public Hilo(String nombre) {
        this.nombre = nombre;
    }

    public Hilo(String nombre, int accion, int vez) {
        this.nombre = nombre;
        this.accion = accion;
        this.vez = vez;
    }

    public Hilo(String nombre, int accion, Archivo archivo, int vez) {
        this.nombre = nombre;
        this.accion = accion;
        this.archivo = archivo;
        this.vez = vez;
    }

    // Metodo del hilo
    public void run() {
        while (this.vez > 0) {
            switch (this.accion) {

                //random
                case 0:
                    this.archivo.println("ID : " + this.getId() + " Nombre : " + this.nombre + "; accion : Random: " + Math.random() * (101) + 1);
                    System.out.println("ID : " + this.getId() + " Nombre : " + this.nombre + "; accion : Random: " + Math.random() * (101) + 1);

                    try {
                        this.sleep(2000);
                    } catch (InterruptedException ex) {
                        System.out.println("\t\tInterupcion a hilo " + this.nombre);
                        this.cambiarRutina();
                        break;
                    }

                    //(int)(Math.random()*(HASTA-DESDE+1)+DESDE); 
                    break;

                //fibonacci
                case 1:
                    this.a = this.b;
                    this.b = this.c;
                    this.c = this.a + this.b;
                    this.archivo.println("ID : " + this.getId() + " Nombre : " + this.nombre + "; accion : FIBONACCI = " + this.c);
                    System.out.println("ID : " + this.getId() + " Nombre : " + this.nombre + "; accion : FIBONACCI = " + this.c);

                    try {
                        this.sleep(2000);
                    } catch (InterruptedException ex) {
                        System.out.println("\t\tInterupcion a hilo " + this.nombre);
                        this.cambiarRutina();
                        break;

                    }

                    break;
                //pares
                case 2:
                    this.par += 2;

                    this.archivo.println("ID : " + this.getId() + " Nombre : " + this.nombre + "; accion : PARES: " + this.par);
                    System.out.println("ID : " + this.getId() + " Nombre : " + this.nombre + "; accion : PARES: " + this.par);

                    try {
                        this.sleep(2000);
                    } catch (InterruptedException ex) {
                        System.out.println("\t\tInterupcion a hilo " + this.nombre);
                        this.cambiarRutina();
                        break;

                    }
                    break;

                //impares
                case 3:
                    this.impar += 2;
                    this.archivo.println("ID : " + this.getId() + " Nombre : " + this.nombre + "; accion : IMPAR: " + this.impar);
                    System.out.println("ID : " + this.getId() + " Nombre : " + this.nombre + "; accion : IMPAR: " + this.impar);

                    try {
                        this.sleep(2000);
                    } catch (InterruptedException ex) {
                        System.out.println("\t\tInterupcion a hilo " + this.nombre);
                        this.cambiarRutina();
                        break;

                    }

                    break;
                //interrupciones
                case 4:
                    this.archivo.println("ID : " + this.getId() + " Nombre : " + this.nombre + "; accion : INTERRUMPIR ");

                    try {
                        this.sleep(1000);
                    } catch (InterruptedException ex) {
                        System.out.println("\t\tInterupcion a hilo " + this.nombre);

                    }
                    for (Hilo hilo : hermanos) {
                        hilo.interrupt();

                    }

                    break;
            }//fin del swich
            this.vez--;
        }//fin del while
    }

    // metodo para poner el boolean a false.
    public void detener() {
        this.setContinuar(false);
    }

    /**
     * @return the continuar
     */
    public boolean isContinuar() {
        return continuar;
    }

    /**
     * @param continuar the continuar to set
     */
    public void setContinuar(boolean continuar) {
        this.continuar = continuar;
    }

    /**
     * @return the rutina
     */
    public boolean isRutina() {
        return rutina;
    }

    /**
     * @param rutina the rutina to set
     */
    public void setRutina(boolean rutina) {
        this.rutina = rutina;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the accion
     */
    public int getAccion() {
        return accion;
    }

    public void imprimir() {
        System.out.println(this.nombre);
    }

    /**
     * @param accion the accion to set
     */
    public void setAccion(int accion, Hilo hilo) {
        hilo.accion = accion;
    }

    public void AgregarHermano(Hilo h) {
        hermanos.add(h);
    }

    public void agregarHermanos(ArrayList<Hilo> hermanos) {
        for (Hilo h : hermanos) {
            this.AgregarHermano(h);
        }
    }

    public void cambiarRutina() {
        this.accion = (this.accion + 2) % 4;
    }

}
