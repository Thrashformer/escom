%{
#include <stdio.h>

#include "tabla.h"
TABLA tabla; 



%}

/* Declaraciones de BISON */
%union {
    int    entero;
    double real;
    char * nombre_var;
}


%token INT
%token FLOAT
%token DOUBLE

%token <real> REAL
%token <entero> ENTERO
%token <nombre_var> VAR

%left '-' '+'
%left '*' '/'

%type <real> expresion
%type <entero> exp_ent

%%    /* Gramatica */

lineas:  /* cadena vacia */
      |  lineas linea
;

linea: '\n'
     | INT VAR '=' exp_ent '\n' {add_variable(&tabla, $2, $4,1);}
     | FLOAT VAR '=' expresion '\n' {add_variable(&tabla, $2, $4,2);}
     | VAR '=' exp_ent '\n' {add_variable(&tabla, $1, $3,1);}
     | VAR '=' expresion '\n' {add_variable(&tabla, $1, $3,2);}
     | expresion '\n'                   { printf ("resultado: %3.1f\n", $1); }
     | exp_ent '\n'                   { printf ("resultado: %d\n", $1); }
     | INT '\n'                       { add_variable(&tabla, "int",0,3);}
     | FLOAT '\n'                     { add_variable(&tabla, "float",0,3);}
     | DOUBLE '\n'                    { add_variable(&tabla, "double",0,3);}
     | error '\n'                     { yyerrok;}



;

expresion: REAL   { $$ = $1; }
         | VAR    { VARIABLE *variable = buscar_variable(&tabla,$1);
                              if (variable != NULL) { /* encontrada */
                                  if (variable->tipo == 1)
                                  { printf(" variable entera ");
                                     $$ = getIntValor(variable);
                                  }
                                  else if (variable->tipo == 2)
                                  {printf(" variable flotante ");
                                     $$ = getDoubleValor(variable);
                                  }
                              }
                              else {
                                 printf("variable %s no definida\n", $1);
                              }
                            }
         | expresion '+' expresion   { $$ = $1 + $3; }
         | expresion '-' expresion   { $$ = $1 - $3; }
         | expresion '*' expresion   { $$ = $1 * $3; }
         | expresion '/' expresion   { $$ = $1 / $3; }
         | exp_ent '+' expresion   { $$ = $1 + $3; }
         | exp_ent '-' expresion   { $$ = $1 - $3; }
         | exp_ent '*' expresion   { $$ = $1 * $3; }
         | exp_ent '/' expresion   { $$ = $1 / $3; }
         | expresion '+' exp_ent   { $$ = $1 + $3; }
         | expresion '-' exp_ent   { $$ = $1 - $3; }
         | expresion '*' exp_ent   { $$ = $1 * $3; }
         | expresion '/' exp_ent   { $$ = $1 / $3; }


;

exp_ent: ENTERO   { $$ = $1; }
         | exp_ent '+' exp_ent   { $$ = $1 + $3; }
         | exp_ent '-' exp_ent   { $$ = $1 - $3; }
         | exp_ent '*' exp_ent   { $$ = $1 * $3; }
         | exp_ent '/' exp_ent   { $$ = $1 / $3; }
         | VAR    { VARIABLE *variable = buscar_variable(&tabla,$1);
                              if (variable != NULL) { /* encontrada */
                                  if (variable->tipo == 1)
                                  { printf(" variable entera ");
                                     $$ = getIntValor(variable);
                                  }
                                  else if (variable->tipo == 2)
                                  {printf(" variable flotante ");
                                     $$ = getDoubleValor(variable);
                                  }
                              }
                              else {
                                 printf("variable %s no definida\n", $1);
                              }
                            }


;




%%

int main(int argc, char** argv) {
    crea_tabla(&tabla);
    yyparse();
    borra_tabla(&tabla);
}

yyerror (char *s) { printf ("%s\n", s);  }
int yywrap()  { return 1; }
