#ifndef _TABLA_H
#define _TABLA_H




#define SI 1
#define NO 0

#define MAX 100
/*
1 = int
2 = float
2 = reservada
*/

struct variable {
	char * nombre;
	double valor;
	int valor_1;
	int tipo; 
};




typedef struct variable VARIABLE;

struct tabla {
	int tamano;
	VARIABLE * variables;
};

typedef struct tabla TABLA;


/* Interfaz de las funciones sobre diccionarios */

void crea_tabla(TABLA *t);
void borra_tabla(TABLA *t);
void add_variable(TABLA *t, char * nombre, double valor, int tipo);
VARIABLE * buscar_variable(TABLA *t, char * nombre);
void eliminar_variable(TABLA *t, char * nombre);
void imprimir_variables(TABLA *t);
void imprimir_variable(VARIABLE *v);
int getIntValor(VARIABLE* var);
double getDoubleValor(VARIABLE* var);

#endif  /* _TABLA_H */
