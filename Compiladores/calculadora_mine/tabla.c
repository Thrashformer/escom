#include <stdlib.h>
#include <stdio.h>
#include <malloc.h>
#include <string.h>

#include "tabla.h"

void crea_tabla(TABLA *d) {
	d->tamano = 0;
	d->variables = (VARIABLE *) malloc(MAX * sizeof(VARIABLE));
	if (d->variables == NULL) {
		printf("No hay memoria para crear la tabla u.u\n");
		exit(1);
	}
}

void borra_tabla(TABLA *d) {
	free(d->variables);
	d->tamano = 0;
}

void add_variable(TABLA *d, char * nombre, double valor, int tipo) {
	VARIABLE * var = buscar_variable(d, nombre);

	if (var != NULL) {
			var->valor = valor;
			return;
	}
	else {
		if (d->tamano < MAX) {
			if (tipo ==1)
			{
			d->variables[d->tamano].nombre = nombre;
			d->variables[d->tamano].valor_1 = (int)valor;
			d->variables[d->tamano].valor = valor;
				d->variables[d->tamano].tipo = 1; // int
				printf("  Se inserto variable %s :%3.0f de tipo entero \n",nombre,valor);
			}
			else if (tipo == 2)
			{
			d->variables[d->tamano].nombre = nombre;
			d->variables[d->tamano].valor = valor;
				d->variables[d->tamano].tipo = 2; // double
				printf("  Se inserto variable %s :%3.2f de tipo flotante \n",nombre,valor);
			}
			else
			{
			d->variables[d->tamano].nombre = nombre;
			d->variables[d->tamano].valor = valor;
				d->variables[d->tamano].tipo = 3; // reservada
				printf("  Se inserto la palabra reservada %s \n",nombre);
			}

			d->tamano++;
		}
		else {
			printf("tabla llena D= \n");
			exit(1);
		}
	}
}


VARIABLE * buscar_variable(TABLA *d, char * nombre) {
	int i;
	for (i=0; i < d->tamano; i++){
		if (strcmp(d->variables[i].nombre, nombre)==0){ 
			return(d->variables+i);
		}
	}
	return (NULL);
}

void eliminar_variable(TABLA *d, char * nombre) {
	int encontrada = NO;
	int i;
	for (i=0; i < d->tamano; i++){
		if (!encontrada) {
			if (strcmp(d->variables[i].nombre, nombre)==0){ 
				encontrada = SI;
			}
		}
		else {
			d->variables[i-1].nombre = d->variables[i].nombre;
			d->variables[i-1].valor = d->variables[i].valor;
		}
	}
	if (encontrada) {
		d->tamano--;
	}
}


void imprimir_variables(TABLA *d) {
	int i;
	for (i=0; i < d->tamano; i++){
		printf("\t%s -> %f\n", d->variables[i].nombre, d->variables[i].valor);
	}
}


void imprimir_variable(VARIABLE* var){
	printf("\t%s : %f\n", var->nombre , var->valor);
}




int getIntValor(VARIABLE* var)
{
	return (var->valor_1);
}

double getDoubleValor(VARIABLE* var)
{
	return (var->valor);
}
