%{
#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include "calculadora.tab.h"

 /* 

INT ^int$
FLOAT ^float$
DOUBLE ^double$
 */

%}

INT int
FLOAT float
DIG [0-9]
PAL  [A-Za-z]

%%
{DIG}+             { 
                        yylval.entero = atoi(yytext);
                        return (ENTERO);
                      }
                      
{DIG}+\.{DIG}+  { yylval.real = atof(yytext);
                        return (REAL);
                      }
                      
"+"|"-"|"*"|"/"|"="   { return (yytext[0]); }
"\n"                  { return (yytext[0]); }


{INT}                  { yylval.real = atof(yytext);
                        return (INT);
                      }

{FLOAT}                  { yylval.real = atof(yytext);
                        return (FLOAT);
                      }


    {PAL}({PAL}|_)*   { yylval.nombre_var = (char *) malloc (strlen(yytext) + 1);
                        strcpy(yylval.nombre_var, yytext);
                        return (VAR); 
                      }



.                     ;

%%