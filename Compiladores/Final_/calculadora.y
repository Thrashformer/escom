%{
#include <math.h>
#include <stdio.h>
#include "libreria_tabla.h"
%}
             
/* Declaraciones de BISON 
		EN ESTE ARCHIVO VA LA LIBRERIA PARA LA ESTRUCTURA
*/
%union{
	int entero;
	double decimal;
	Variable variable;
}

%token IGUAL
%token <entero> ENTERO
%token <decimal> DECIMAL
%token <variable> VARIABLE
%type <entero> exp_entera
%type <decimal> exp_decimal
%type <variable> exp_variable

             
%left '-' '+'
%left '*' '/'
             
/* Gramática */
%%
             
input:    /* cadena vacía */
        | input line             
;

line:     '\n'
        | exp_entera '\n'  { printf ("\tresultado %d\n", $1); }
        | exp_decimal '\n'  { printf ("\tresultado %3.2f\n", $1); }
;

exp_entera:     ENTERO	{ $$ = $1; }
	| exp_entera '+' exp_entera     { $$ = $1 + $3;    }
	| exp_entera '-' exp_entera     { $$ = $1 - $3;    }
	| exp_entera '*' exp_entera     { $$ = $1 * $3;    }
	| exp_entera '/' exp_entera     { $$ = $1 / $3;    }
;

exp_decimal:     DECIMAL	{ $$ = $1; }
	| exp_decimal '+' exp_decimal   { $$ = $1 + $3;    }
	| exp_decimal '-' exp_decimal   { $$ = $1 - $3;    }
	| exp_decimal '*' exp_decimal   { $$ = $1 * $3;    }
	| exp_decimal '/' exp_decimal   { $$ = $1 / $3;    }
	| exp_decimal '+' exp_entera    { $$ = $1 + $3;    }
	| exp_entera  '+' exp_decimal   { $$ = $1 + $3;    }
	| exp_decimal '-' exp_entera    { $$ = $1 - $3;    }
	| exp_entera  '-' exp_decimal   { $$ = $1 - $3;    }
	| exp_decimal '*' exp_entera    { $$ = $1 * $3;    }
	| exp_entera  '*' exp_decimal   { $$ = $1 * $3;    }
	| exp_decimal '/' exp_entera    { $$ = $1 / $3;    }
	| exp_entera  '/' exp_decimal   { $$ = $1 / $3;    }
;             

exp_variable:     VARIABLE	{ $$ = $1; }
	| exp_variable '+' exp_variable   { $$ = $1 + $3;    }
;
%%

int main() {
  yyparse();
}
             
yyerror (char *s)
{
  printf ("%s\n", s);
}

             
int yywrap()  
{  
  return 1;  
}  
