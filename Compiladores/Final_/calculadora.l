%{
#include "calculadora.tab.h"
#include "libreria_tabla.h"

%}
/*#include "calculadora.tab.h"*/

NUM [0-9]+
DEC {NUM}+"."{NUM}{1,}
VAR ^[a-zA-Z_]{1}[a-zA-Z1-9_-]*
%%

{NUM}           {
		printf("Numero entero %s\n",yytext);
                yylval.entero = atoi(yytext);
                return (ENTERO);
                }
{DEC}           {
		printf("Numero decimal %s\n",yytext);
                yylval.decimal = atof(yytext);
                return (DECIMAL);
                }
{VAR}           {
                printf("Variable %s\n",yytext);
                yylval.variable = atof(yytext);
                return (VARIABLE);
                }


"+"	 	{
                return (yytext[0]);
                }
"-"	 	{
                return (yytext[0]);
                }
"*"	 	{
                return (yytext[0]);
                }
"/"	 	{
                return (yytext[0]);
                }

"\n"            {
                return (yytext[0]);
                }
"="             {printf("Asignacion %s\n",yytext); 
                return(IGUAL);}

.               ;
%%
