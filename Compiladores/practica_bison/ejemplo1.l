%{
#include "ejemplo1.tab.h"
%}

NUM [0-9]+
DEC {NUM}+"."{NUM}{1,}
%%

{NUM}           {
		printf("Numero entero %s\n",yytext);
                yylval.entero = atoi(yytext);
                return (ENTERO);
                }
{DEC}           {
		printf("Numero decimal %s\n",yytext);
                yylval.decimal = atof(yytext);
                return (DECIMAL);
                }

"+"	 	{
                return (yytext[0]);
                }
"-"	 	{
                return (yytext[0]);
                }
"*"	 	{
                return (yytext[0]);
                }
"/"	 	{
                return (yytext[0]);
                }

"\n"            {
                return (yytext[0]);
                }
.               ;
%%
