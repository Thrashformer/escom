#!/usr/bin/env python
# -*- coding: utf-8 -*-
from models.models import *


#Multiplicar las funciones
def multiplicar(funcion1, funcion2):
    tam_func1= funcion1.tamanio
    tam_func2=funcion2.tamanio

    if tam_func1 > tam_func2:
        print('caso funcion 1 > funcion2 ')
        print('posicion inicial1 ', funcion1.inicial)
        print('posicion inicial2 ', funcion2.inicial)
        posicion_inicial_1= funcion1.posicion_inicial
        posicion_inicial_2= funcion2.posicion_inicial
        #Creamos la matriz de ceros para guardar los datos
        resultado=[[0 for i in range(tam_func1+j)] for j in range(tam_func2) ]
        imprimir_matriz(resultado)
        #resultado=[]
        for i in range(tam_func2):
            for j in range(tam_func1):
                resultado[i][j+i]= funcion2.funcion[i]*funcion1.funcion[j]
        imprimir_matriz(resultado)

        return funcion_convolucion(resultado, posicion_inicial_1+posicion_inicial_2)


    elif tam_func2 > tam_func1:
        print('caso funcion 1 < funcion2 ')
        print('posicion inicial1 ', funcion1.inicial)
        print('posicion inicial2 ', funcion2.inicial)
        posicion_inicial_1= funcion1.posicion_inicial
        posicion_inicial_2= funcion2.posicion_inicial
        #funcion2 es mas grande
        resultado=[[0 for i in range(tam_func2+j)] for j in range(tam_func1) ]
        imprimir_matriz(resultado)
        #resultado=[]
        for i in range(tam_func1):
            for j in range(tam_func2):
                resultado[i][j+i]= funcion1.funcion[i]*funcion2.funcion[j]
        imprimir_matriz(resultado)
        return funcion_convolucion(resultado, posicion_inicial_1+posicion_inicial_2)

    else:
        print('caso funcion 1 == funcion2 ')
        print('posicion inicial1 ', funcion1.inicial)
        print('posicion inicial2 ', funcion2.inicial)
        posicion_inicial_1= funcion1.posicion_inicial
        posicion_inicial_2= funcion2.posicion_inicial
        #las funciones son del mismo tamanio
        resultado=[[0 for i in range(tam_func1+j)] for j in range(tam_func2) ]
        imprimir_matriz(resultado)
        #resultado=[]
        for i in range(tam_func2):
            for j in range(tam_func1):
                resultado[i][j+i]= funcion2.funcion[i]*funcion1.funcion[j]
        imprimir_matriz(resultado)
        return funcion_convolucion(resultado, posicion_inicial_1+posicion_inicial_2)

    return resultado


#funcion de convolucion periodica
def convolucion_periodica(funcion1, funcion2):
    #resultado de multiplicar las matrices
    objeto_convolucion = multiplicar(funcion1, funcion2)
    imprimir_matriz(objeto_convolucion.funcion)
    print('funcion zip ...')
    r2 = zip(*objeto_convolucion.funcion)
    r3 = list()
    for i in range(len(r2)):
        r3.append(sum(r2[i]))

    imprimir_matriz(r2)
    print(r3)
    imprimir_matriz(objeto_convolucion.funcion)
    return reducir(r3)

terminar = False
while (not terminar):
    #graficar('')

    #grafica_discreta()
    #'''
    print('ya grafico')
    #f = raw_input('ingresa la funcion uno, cada numero separado por comas')
    #ini= raw_input ('cual es la posicion inicial?')
    '''
    f = [3,0,-2,3,0,-2,3,0,-2]
    ini = 2
    funcion1=funcion_convolucion(f, ini)
    #funcion_1, inicio_1 = to_funcion(f,ini)
    #f = raw_input('ingresa la funcion dos, cada numero separado por comas')
    #ini= raw_input ('cual es la posicion inicial?')
    f = [4,1,5,6]
    ini = 1
    funcion2=funcion_convolucion(f, ini)
    #funcion_2, inicio_2 = to_funcion(f,ini)
    #grafica_discreta(f, inicio=ini)
    '''
    f = [1,2,1,2]
    ini = 0
    funcion1=funcion_convolucion(f, ini)
    #funcion_1, inicio_1 = to_funcion(f,ini)
    #f = raw_input('ingresa la funcion dos, cada numero separado por comas')
    #ini= raw_input ('cual es la posicion inicial?')
    f = [3,6,4,2]
    ini = 1
    funcion2=funcion_convolucion(f, ini)
    #funcion_2, inicio_2 = to_funcion(f,ini)
    #grafica_discreta(f, inicio=ini)

    print (funcion1)
    print (funcion2)
    if funcion1.isPeriodica() and funcion2.isPeriodica():
        #Caso de la convolucion circular
        Convolucionar = convolucion(funcion1,funcion2,CIRCULAR)
        pass
    else:
        #una de las dos no es periodica
        if funcion1.periodica:
            #funcion1.reducir_funcion_periodica()
            Convolucionar = convolucion(funcion1,funcion2,PERIODICA)
            pass
        else:
            #funcion2.reducir_funcion_periodica()
            Convolucionar = convolucion(funcion1,funcion2,DISCRETA)
            pass

        #resultado = convolucion_periodica(funcion1, funcion2)
        resultado = convolucion_periodica(funcion2, funcion1)
        
        print('funcion a graficar')
        print(resultado)
        try:
            grafica_discreta(funcion=resultado, inicio=0)
        except Exception as e:
            print(e)
            pass


        pass
    #'''
    terminar=True
print('termino..')


