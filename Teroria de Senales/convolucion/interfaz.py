__author__ = 'metallica'
from models.funciones import grafica_discreta, to_lista
from models.models import *

from Tkinter import *
import tkMessageBox


def hacer_calculos():
    f1 = [1,2,1,2]
    inicio1 = 0
    funcion_1 = funcion_convolucion(funcion=f1,posicion_inicial=inicio1)

    f2 = [3,6,4,2]
    inicio2 = 1
    funcion_2 = funcion_convolucion(funcion=f2,posicion_inicial=inicio2)

    tipo_operacion = PERIODICA
    conv = convolucion(funcion_1 , funcion_2 , tipo_operacion=tipo_operacion)

    resultado = conv.convolucion_discreta()
    grafica_discreta(resultado,0)



def AccionCalcular():
    if ( len(funcion1.get())==0 or len(funcion2.get())==0 ):
    	tkMessageBox.showinfo("ERROR","Error no se ha llenado todos los campos")
    else:
        #try:
        if True:
            f1 = funcion1.get()
            f2 = funcion2.get()
            if inicioF1.get() != '':
                inicio1 = int(inicioF1.get())
            else:
                inicio1=0
            if inicioF2.get() != '':
                inicio2 = int(inicioF2.get())
            else:
                inicio2=0
            if v.get() != '':
                opc = int(v.get())
            else:
                opc=-1

            #tkMessageBox.showinfo("Calculando", ("Usted escogio la opcion: " + str(opc)))
            funcion_1 = funcion_convolucion(funcion=to_lista(f1),posicion_inicial=inicio1)
            funcion_2 = funcion_convolucion(funcion=to_lista(f2),posicion_inicial=inicio2)

            if opc == -1 :
                tipo_operacion = getOperacion(funcion_1 , funcion_2)
            else:
                flag,operacion  = validar_Operacion(funcion_1,funcion_2)
                if flag:
                    tipo_operacion = opc
                else:
                    tipo_operacion = operacion

            print (funcion_1)
            print (funcion_2)
            conv = convolucion(funcion_1 , funcion_2 , tipo_operacion=tipo_operacion)
            print(conv)
            try:
                if conv.tipo_operacion == DISCRETA:
                    resultado,inicio = conv.convolucion_discreta()
                    grafica_discreta(resultado,inicio)
                elif conv.tipo_operacion == PERIODICA:
                    resultado,inicio = conv.convolucion_periodica()
                    grafica_discreta(resultado,inicio)
                elif conv.tipo_operacion == CIRCULAR:
                    resultado,inicio = conv.convolucion_circular()
                    grafica_discreta(resultado,inicio)
            except Exception as e:
                print('Up!! D= ',e)


        #except Exception as e:
        #    print(e)

#v = 0
v0 = Tk() #Ventana Principal
v = StringVar() 
v0.geometry("750x300") #TAMANO DE VENTANA PRINCIPAL
#BOTONES
Calcular=Button(v0,text="Calcular", command=AccionCalcular)
#boton=Button(v0, text="click aqui", command=accion_boton)
#LABELS
f1=Label(v0,text="Funcion 1")
f2=Label(v0,text="Funcion 2")
i1=Label(v0,text="    Pocision inicial 1")
i2=Label(v0,text="    Pocision inicial 2")
#CAJAS DE TEXTO
variable_string = StringVar()
variable_string2 = StringVar()
IF1 = StringVar()
IF2 = StringVar()
variable_string3 = StringVar()
funcion1= Entry(v0,textvariable=variable_string)
funcion2= Entry(v0,textvariable=variable_string2)
inicioF1= Entry(v0,textvariable=IF1)
inicioF2= Entry(v0,textvariable=IF2)
#AGREGANDO LOS ELEMENTOS A LA VENTANA
f1.grid(row=1,column=1)
funcion1.grid(row=1,column=2)
f2.grid(row=2,column=1)
funcion2.grid(row=2,column=2)
i1.grid(row=1,column=3)
inicioF1.grid(row=1,column=4)
i2.grid(row=2,column=3)
inicioF2.grid(row=2,column=4)
Calcular.grid(row=5,column=2)
#boton.grid(row=10,column=10)

Label(v0, 
      text="""Seleccione su tipo de funcion:""",
      #justify = LEFT,
      padx = 20).grid(row=3,column=2)
Radiobutton(v0, 
            text="Periodica",
            padx = 20, 
            variable=v, 
            value=1).grid(row=4,column=1)
Radiobutton(v0, 
            text="Discreta",
            padx = 20, 
            variable=v, 
            value=2).grid(row=4,column=2)
Radiobutton(v0, 
            text="Circular",
            padx = 20, 
            variable=v, 
            value=3).grid(row=4,column=3)


def correr_interfaz():
      v0.mainloop()
