from .funciones import *

DISCRETA=1
PERIODICA = 2
CIRCULAR = 3

#   Obtener que tipo de convolucion es
def getOperacion(funcion_1 , funcion_2):
    if funcion_1.isPeriodica() and funcion_2.isPeriodica():
        return CIRCULAR
    elif not funcion_1.isPeriodica() and not funcion_2.isPeriodica():
        return DISCRETA
    return PERIODICA

class funcion_convolucion():
    funcion = []
    periodica = False
    posicion_inicial = 0
    inicial={}
    tamanio =-1
    def __init__(self, funcion, posicion_inicial=0):
        self.funcion = funcion
        if self.isPeriodica():
            self.periodica = True
        if posicion_inicial < 0 :
            self.posicion_inicial=0
        elif len(funcion) < posicion_inicial:
            self.posicion_inicial=len(funcion)
        else:
            self.posicion_inicial = posicion_inicial
        self.inicial = {self.posicion_inicial:self.funcion[self.posicion_inicial]}
        self.tamanio = len(self.funcion)

    def isPeriodica(self):
        nueva_func=[]
        contador=0
        if self.tamanio <3:
            self.periodica=False
            return self.periodica
        for i in self.funcion:
            if self.funcion.count(i) > 1:
                contador+=1
        if contador>1:
            self.periodica=True
            return self.periodica
        self.periodica=False
        return self.periodica

    def reducir_funcion_periodica(self):
        nueva_func=[]
        for i in self.funcion:
            if not i in nueva_func:
                nueva_func.append(i)
        self.funcion=nueva_func
        self.tamanio=len(self.funcion)
        print('se redujo la funcion periodica..a ',self.funcion)


    def __str__(self):
        return str((str(self.funcion) + " posicion_inicial:%s Periodica: %s " % (self.posicion_inicial, self.periodica)))

#   Guardara las Dos funciones
#   QQue tipo de operacion hara y demas
class convolucion():
    funcion_1= []
    funcion_2 = []
    # 0=Discreta, 1=periodica , 2 =Circular
    tipo_operacion = 1

    def __init__(self, funcion_1=None, funcion_2=None, tipo_operacion=-1):
        self.funcion_1 = funcion_1
        self.funcion_2 = funcion_2
        if tipo_operacion == -1 :
            self.tipo_operacion=getOperacion(funcion_1 , funcion_2)


    def __str__(self):
        return str('Convolucionaremos a \nFuncion 1 : '+ str(self.funcion_1)
                   + '\nFuncion 2 : ' + str(self.funcion_2)
                   + 'tipo de Operacion : ' + str(self.tipo_operacion))

    def convolucion_discreta(self):
        #resultado de multiplicar las matrices
        objeto_convolucion = self.multiplicar()
        imprimir_matriz(objeto_convolucion.funcion)
        print('funcion zip ...')
        r2 = zip(*objeto_convolucion.funcion)
        r3 = list()
        for i in range(len(r2)):
            r3.append(sum(r2[i]))

        imprimir_matriz(r2)
        print(r3)
        imprimir_matriz(objeto_convolucion.funcion)
        return r3 , objeto_convolucion.posicion_inicial


    def convolucion_periodica(self):
        #resultado de multiplicar las matrices
        objeto_convolucion = self.multiplicar()
        imprimir_matriz(objeto_convolucion.funcion)
        print('funcion zip ...')
        r2 = zip(*objeto_convolucion.funcion)
        r3 = list()
        for i in range(len(r2)):
            r3.append(sum(r2[i]))

        imprimir_matriz(r2)
        print(r3)
        imprimir_matriz(objeto_convolucion.funcion)
        return reducir(r3) , objeto_convolucion.posicion_inicial

    def convolucion_circular(self):
        #resultado de multiplicar las matrices
        objeto_convolucion = self.multiplicar()
        imprimir_matriz(objeto_convolucion.funcion)
        print('funcion zip ...')
        r2 = zip(*objeto_convolucion.funcion)
        r3 = list()
        for i in range(len(r2)):
            r3.append(sum(r2[i]))

        imprimir_matriz(r2)
        print(r3)
        imprimir_matriz(objeto_convolucion.funcion)
        return reducir(r3) , objeto_convolucion.posicion_inicial


    #Multiplicar las funciones
    def multiplicar(self):
        tam_func1 = self.funcion_1.tamanio
        tam_func2 = self.funcion_2.tamanio

        if tam_func1 > tam_func2:
            print('caso funcion 1 > funcion2 ')
            posicion_inicial_1= self.funcion_1.posicion_inicial
            posicion_inicial_2= self.funcion_2.posicion_inicial
            #Creamos la matriz de ceros para guardar los datos
            resultado=[[0 for i in range(tam_func1+j)] for j in range(tam_func2) ]
            imprimir_matriz(resultado)
            #resultado=[]
            for i in range(tam_func2):
                for j in range(tam_func1):
                    resultado[i][j+i]= self.funcion_2.funcion[i] * self.funcion_1.funcion[j]
            imprimir_matriz(resultado)
            return funcion_convolucion(resultado, posicion_inicial_1+posicion_inicial_2)


        elif tam_func2 > tam_func1:
            print('caso funcion 1 < funcion2 ')
            posicion_inicial_1= self.funcion_1.posicion_inicial
            posicion_inicial_2= self.funcion_2.posicion_inicial
            #funcion2 es mas grande
            resultado=[[0 for i in range(tam_func2+j)] for j in range(tam_func1) ]
            imprimir_matriz(resultado)
            #resultado=[]
            for i in range(tam_func1):
                for j in range(tam_func2):
                    resultado[i][j+i]= self.funcion_1.funcion[i] * self.funcion_2.funcion[j]
            imprimir_matriz(resultado)
            return funcion_convolucion(resultado, posicion_inicial_1+posicion_inicial_2)

        else:
            print('caso funcion 1 == funcion2 ')
            posicion_inicial_1= self.funcion_1.posicion_inicial
            posicion_inicial_2= self.funcion_2.posicion_inicial
            #las funciones son del mismo tamanio
            resultado=[[0 for i in range(tam_func1+j)] for j in range(tam_func2) ]
            imprimir_matriz(resultado)
            #resultado=[]
            for i in range(tam_func2):
                for j in range(tam_func1):
                    resultado[i][j+i]= self.funcion_2.funcion[i] * self.funcion_1.funcion[j]
            imprimir_matriz(resultado)
            return funcion_convolucion(resultado, posicion_inicial_1+posicion_inicial_2)

        return resultado




