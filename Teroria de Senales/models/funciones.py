#!/usr/bin/env python
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np
from numpy import array

__author__ = 'metallica'
DISCRETA=1
PERIODICA = 1
CIRCULAR = 2


def imprimir_matriz(funcion):
    tam=len(funcion)
    print('___________')
    for i in range(tam):
        print (funcion[i])
    print('**************')



#   Obtener que tipo de convolucion es
def validar_Operacion(funcion_1 , funcion_2, op_selecionada):
    if funcion_1.isPeriodica() and funcion_2.isPeriodica():
        if op_selecionada == CIRCULAR:
            return True, CIRCULAR
        return False,None
    elif not funcion_1.isPeriodica() and not funcion_2.isPeriodica():
        if op_selecionada == DISCRETA:
            return True,DISCRETA
        return False,None
    if op_selecionada == PERIODICA:
        return True,PERIODICA
    return False,None



#Para reducir las funciones que son periodicas
def reducir(funcion):
    resultado=list()
    for i in funcion:
        if funcion.count(i)>1:
            resultado.append(i)
    return resultado

#Retorna la Funcion sin  el simbolo que los separa y su posicion inicial
def to_funcion(funcion,inicial):
    resultado=[]
    try:
        for f in funcion.replace(' ','').split(','):
            resultado.append(f)
        return resultado, int(inicial)
    except:
        return None, None

#Retorna la Funcion sin  el simbolo que los separa y su posicion inicial
def to_lista(funcion):
    resultado=[]
    try:
        for f in funcion.replace(' ','').split(','):
            resultado.append(int(f))
        return resultado
    except:
        return None

#Grafica dummie
def graficar (funcion):

    # Creamos el array x de cero a cien con cien puntos
    x = np.linspace(-10, 10, 10)

    # Creamos el array y conde cada punto es el seno de cada elemento de x
    y = np.sin(funcion)
    # a = array( [2,3,4] )
    y= array(funcion)

    # Creamos una figura
    plt.figure()

    # Establecer el color de los ejes.
    plt.axhline(0, color="black")
    plt.axvline(0, color="black")

    # Limitar los valores de los ejes.
    plt.xlim(-10, 10)
    plt.ylim(-10, 10)

    # Representamos

    plt.plot(x,y,marker='.' )

    # Mostramos en pantalla
    plt.show()

#Funcion para graficar la funcion ya transformada
def grafica_discreta(funcion=[1,-1,1,-1,1,-1], inicio=0):
    #plt.plot([1,2,3,4], [1,4,9,16], 'ro')
    #plot (puntos_en_x , puntos_en_y )
    print(funcion)
    tam_x = len(funcion)
    x = [(i-inicio) for i in range(tam_x)]

    # Establecer el color de los ejes.
    plt.axhline(0, color="black")
    plt.axvline(0, color="black")

    plt.grid(color='b', linestyle='--')
    # graficando
    plt.plot(x, funcion, 'ro' )
    # aix [xmin , xmax , ymin , ymax]
    plt.axis([ x[0]-2 , x[-1]+2, min(funcion)-2, max(funcion)+2 ])
    plt.show()

